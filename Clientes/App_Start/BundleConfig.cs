﻿using System.Web;
using System.Web.Optimization;

namespace Clientes
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery.unobtrusive-ajax.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/mvcfoolproof").Include(
                        "~/Scripts/MicrosoftAjax.js",
                        "~/Scripts/MicrosoftMvcAjax.js",
                        "~/Scripts/MicrosoftMvcValidation.js",
                        "~/ClientScripts/mvcfoolproof.unobtrusive.min.js",
                        "~/ClientScripts/MvcFoolproofJQueryValidation.min.js",
                        "~/ClientScripts/MvcFoolproofValidation.min.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/gridmvc.js",
                      "~/Scripts/bootstrap-datepicker.js",
                      "~/Scripts/gridmvc.customwidgets.js",
                      "~/Scripts/gridmvc.lang.es.js",
                      //"~/Scripts/foundation.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/gridmvc.datepicker.css",
                      "~/Content/Gridmvc.css",
                      //"~/Content/foundation.css",
                      "~/Content/site.css"));
        }
    }
}
