﻿using Clientes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Clientes.Controllers
{
    public class ActividadesController : Controller
    {
        private EmpresarialesModel db = new EmpresarialesModel();
        // GET: Actividades
        public ActionResult Index(string id)
        {
            if (!String.IsNullOrEmpty(id))
            {
                ViewBag.vend = id;
            }

            ViewData["ejecutivos"] = new SelectList(db.COUSUARIOS.ToList().Where(x => x.ACTIVO = true && x.PERFIL != 1), "VENDEDOR", "NOMBRE");
            return View();
        }



        public JsonResult Eventos(string id, DateTime start, DateTime end)
        {
            var eventos = db.Database.SqlQuery<Events>("SELECT V.ID id,CONVERT(varchar,V.start) start,v.color,v.t1 + '-' + CN.NOMBRE_COMERCIAL + '-' + V.Descripcion title,CN.EJECUTIVO_FONIX ejecutivo,CONVERT(bit,V.allDay) allDay,CASE WHEN CONVERT(DATE,V.start)=CONVERT(DATE,(SELECT MAX(c1.FECHA_CONTACTO) FROM EMPRESARIALES.CONTACTO c1 WHERE c1.CONOCIMIENTO_ID=V.IDCON)) THEN 'CUMPLIDO' WHEN (CONVERT(DATE,V.start)>CONVERT(DATE,(SELECT MAX(c1.FECHA_CONTACTO) FROM EMPRESARIALES.CONTACTO c1 WHERE c1.CONOCIMIENTO_ID=V.IDCON)) AND CONVERT(DATE,V.start)>CONVERT(DATE,GETDATE())) OR (CONVERT(DATE,v.start)=CONVERT(DATE,GETDATE())) THEN 'VIGENTE' ELSE 'INCUMPLIDO' END AS estatus FROM (SELECT CP.ID,CP.FECHA_COMPROMISO start,'Compromiso' t1,CONVERT(varchar,CP.COMPROMISO) Descripcion,C.CONOCIMIENTO_ID IDCON,C.FECHA_CONTACTO,'#56a0d3' color,'true' allDay FROM EMPRESARIALES.CONTACTO C JOIN EMPRESARIALES.COMPROMISO CP ON C.ID=cp.CONTACTO_ID UNION SELECT PC.ID,PC.FECHA_PROXIMO_CONTACTO start,'ProximoContacto' t1,CONVERT(varchar,PC.MOTIVO_PROXIMO_CONTACTO) Descripcion,C.CONOCIMIENTO_ID IDCON,C.FECHA_CONTACTO,'#8ec06c' color,'false' allDay FROM EMPRESARIALES.CONTACTO C JOIN EMPRESARIALES.PROXIMO_CONTACTO PC ON C.ID=PC.CONTACTO_ID)V JOIN EMPRESARIALES.CONOCIMIENTO CN ON V.IDCON = cn.ID").ToList();

            List<Events> Evs = new List<Events>();
            foreach(var i in eventos)
            {
                Events ev = new Events();
                ev.id = i.id;
                ev.start = Convert.ToDateTime(i.start).ToString("s", System.Globalization.CultureInfo.InvariantCulture);
                ev.title = i.title + " - " + i.estatus;
                ev.estatus = i.estatus;
                ev.color = i.color;
                ev.ejecutivo = i.ejecutivo;
                //ev.borderColor = "#7f181b";
                ev.className = i.estatus;
                ev.allDay = i.allDay;
                Evs.Add(ev);
            }


            return Json(Evs, JsonRequestBehavior.AllowGet);
        }
    }
}