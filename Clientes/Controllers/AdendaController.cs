﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Clientes.Models;

namespace Clientes.Controllers
{
    public class AdendaController : Controller
    {
        private EmpresarialesModel db = new EmpresarialesModel();



        public ActionResult Lineas()
        {
            var id = Convert.ToInt16(Request.QueryString["id"]);
            var razon = db.CONOCIMIENTO.Find(id);
            var regs = db.ADENDAs.Where(x => x.RFC==razon.RFC);
            ViewBag.RazonSocial = razon.RAZON_SOCIAL;
            ViewBag.Adenda = id;
            return View(regs.ToList());
            //return PartialView("_Lineas",regs);
        }






        // GET: Adenda
        public async Task<ActionResult> Index()
        {
            return View(await db.ADENDAs.ToListAsync());
        }

        // GET: Adenda/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ADENDA aDENDA = await db.ADENDAs.FindAsync(id);
            if (aDENDA == null)
            {
                return HttpNotFound();
            }
            return View(aDENDA);
        }

        // GET: Adenda/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Adenda/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,TIPO,REGION,CUENTA_PADRE,CUENTA,CICLO,RFC,RAZON_SOCIAL,TELEFONO,ICCID,IMEI,ESTATUS_CUENTA,PRODUCTO_CC,CLAVE_PLAN,NOMBRE_PLAN,MONTO_RENTA,EQUIPO,MODELO,DURACION,FECHA_INICIO,FECHA_TERMINO,ESTATUS_ADENDUM,MESES_RESTANTES,EJECUTIVO,GRUPO,CREATEDATE,ESTATUS_LINEA")] ADENDA aDENDA)
        {
            if (ModelState.IsValid)
            {
                db.ADENDAs.Add(aDENDA);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(aDENDA);
        }

        // GET: Adenda/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            var estatusc = new SelectList(new[]
                                          {
                                              new {ID="",Name="--SELECCIONE OPCION"},
                                              new {ID="AC",Name="AC"},
                                              new{ID="TB",Name="TB"},
                                              new{ID="CDD",Name="CAMBIO DE DISTRIBUIDOR"},
                                          },
               "ID", "Name", 1);
            ViewData["estatusc"] = estatusc;


            var estatusl = new SelectList(new[]
                                          {
                                              new {ID="",Name="--SELECCIONE OPCION"},
                                              new {ID="ACTIVA",Name="ACTIVA"},
                                              new{ID="CANCELADA",Name="CANCELADA"},
                                              new{ID="PORT OUT MOVISTAR",Name="PORT OUT MOVISTAR"},
                                              new{ID="PORT OUT AT&T",Name="PORT OUT AT&T"},
                                              new{ID="CESIÓN DE DERECHOS",Name="CESIÓN DE DERECHOS"},
                                          },
               "ID", "Name", 1);
            ViewData["estatusl"] = estatusl;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ADENDA aDENDA = await db.ADENDAs.FindAsync(id);
            if (aDENDA == null)
            {
                return HttpNotFound();
            }
            return View(aDENDA);
        }

        // POST: Adenda/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,TIPO,REGION,CUENTA_PADRE,CUENTA,CICLO,RFC,RAZON_SOCIAL,TELEFONO,ICCID,IMEI,ESTATUS_CUENTA,PRODUCTO_CC,CLAVE_PLAN,NOMBRE_PLAN,MONTO_RENTA,EQUIPO,MODELO,DURACION,FECHA_INICIO,FECHA_TERMINO,ESTATUS_ADENDUM,MESES_RESTANTES,EJECUTIVO,GRUPO,CREATEDATE,ESTATUS_LINEA")] ADENDA aDENDA)
        {
            var estatusc = new SelectList(new[]
                                          {
                                              new {ID="",Name="--SELECCIONE OPCION"},
                                              new {ID="AC",Name="AC"},
                                              new{ID="TB",Name="TB"},
                                          },
               "ID", "Name", 1);
            ViewData["estatusc"] = estatusc;


            var estatusl = new SelectList(new[]
                                          {
                                              new {ID="",Name="--SELECCIONE OPCION"},
                                              new {ID="ACTIVA",Name="ACTIVA"},
                                              new{ID="CANCELADA",Name="CANCELADA"},
                                              new{ID="PORT OUT MOVISTAR",Name="PORT OUT MOVISTAR"},
                                              new{ID="PORT OUT AT&T",Name="PORT OUT AT&T"},
                                              new{ID="CESIÓN DE DERECHOS",Name="CESIÓN DE DERECHOS"},
                                          },
               "ID", "Name", 1);
            ViewData["estatusl"] = estatusl;
            if (ModelState.IsValid)
            {
                db.Entry(aDENDA).State = EntityState.Modified;
                await db.SaveChangesAsync();
                var url = Request.Url.ToString();
                if (url.Contains("201.144.210.148"))
                {
                    return Redirect("Adenda/Lineas?id=" + Request.QueryString["ad"]);
                }
                return Redirect("/Empresariales/Clientes/Adenda/Lineas?id=" + Request.QueryString["ad"]);
            }
            return View(aDENDA);
        }

        // GET: Adenda/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ADENDA aDENDA = await db.ADENDAs.FindAsync(id);
            if (aDENDA == null)
            {
                return HttpNotFound();
            }
            return View(aDENDA);
        }

        // POST: Adenda/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ADENDA aDENDA = await db.ADENDAs.FindAsync(id);
            db.ADENDAs.Remove(aDENDA);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
