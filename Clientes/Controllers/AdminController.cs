﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Clientes.Models;

namespace Clientes.Controllers
{
    public class AdminController : Controller
    {
        private EmpresarialesModel db = new EmpresarialesModel();


        public async Task<ActionResult> EditarUsuario(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            COUSUARIOS cOUSUARIOS = await db.COUSUARIOS.FindAsync(id);
            if (cOUSUARIOS == null)
            {
                return HttpNotFound();
            }
            return View(cOUSUARIOS);
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditarUsuario([Bind(Include = "ID,VENDEDOR,NOMBRE,PASSWORD,ACTIVO,PERFIL")] COUSUARIOS cOUSUARIOS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cOUSUARIOS).State = EntityState.Modified;
                await db.SaveChangesAsync();

                ViewBag.Mensaje = "Contraseña modificada correctamente :)";

                return View(cOUSUARIOS);
            }
            ViewBag.Mensaje = "Ocurrio un error :(";
            return View(cOUSUARIOS);
        }


        // GET: Admin
        public async Task<ActionResult> Index()
        {
            return View(await db.COUSUARIOS.ToListAsync());
        }

        // GET: Admin/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            COUSUARIOS cOUSUARIOS = await db.COUSUARIOS.FindAsync(id);
            if (cOUSUARIOS == null)
            {
                return HttpNotFound();
            }
            return View(cOUSUARIOS);
        }

        // GET: Admin/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,VENDEDOR,NOMBRE,PASSWORD,ACTIVO,PERFIL")] COUSUARIOS cOUSUARIOS)
        {
            if (ModelState.IsValid)
            {
                db.COUSUARIOS.Add(cOUSUARIOS);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(cOUSUARIOS);
        }

        // GET: Admin/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            COUSUARIOS cOUSUARIOS = await db.COUSUARIOS.FindAsync(id);
            if (cOUSUARIOS == null)
            {
                return HttpNotFound();
            }
            return View(cOUSUARIOS);
        }

        // POST: Admin/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,VENDEDOR,NOMBRE,PASSWORD,ACTIVO,PERFIL")] COUSUARIOS cOUSUARIOS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cOUSUARIOS).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(cOUSUARIOS);
        }

        // GET: Admin/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            COUSUARIOS cOUSUARIOS = await db.COUSUARIOS.FindAsync(id);
            if (cOUSUARIOS == null)
            {
                return HttpNotFound();
            }
            return View(cOUSUARIOS);
        }

        // POST: Admin/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            COUSUARIOS cOUSUARIOS = await db.COUSUARIOS.FindAsync(id);
            db.COUSUARIOS.Remove(cOUSUARIOS);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
