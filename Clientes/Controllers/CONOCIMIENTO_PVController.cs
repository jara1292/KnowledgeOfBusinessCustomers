﻿using System.Data.Entity;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using Clientes.Models;

namespace Clientes.Controllers
{
    public class CONOCIMIENTO_PVController : Controller
    {
        private EmpresarialesModel db = new EmpresarialesModel();

        // GET: CONOCIMIENTO_PV
        public async Task<ActionResult> Index()
        {
            var cONOCIMIENTO_PV = db.CONOCIMIENTO_PV.Include(c => c.CONOCIMIENTO);
            return View(await cONOCIMIENTO_PV.ToListAsync());
        }

        // GET: CONOCIMIENTO_PV/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CONOCIMIENTO_PV cONOCIMIENTO_PV = await db.CONOCIMIENTO_PV.FindAsync(id);
            if (cONOCIMIENTO_PV == null)
            {
                return HttpNotFound();
            }
            return View(cONOCIMIENTO_PV);
        }

        // GET: CONOCIMIENTO_PV/Create
        public ActionResult Create()
        {
            ViewBag.CONOCIMIENTO_ID = new SelectList(db.CONOCIMIENTO, "ID", "TIPO");
            return View();
        }

        // POST: CONOCIMIENTO_PV/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CONOCIMIENTO_PV CONOCIMIENTO_PV)
        {
            if (ModelState.IsValid)
            {
                db.CONOCIMIENTO_PV.Add(CONOCIMIENTO_PV);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");

                //foreach(var item in CONOCIMIENTO_PV.CONOCIMIENTO_PV)
                //{

                //}
            }

            ViewBag.CONOCIMIENTO_ID = new SelectList(db.CONOCIMIENTO, "ID", "TIPO", CONOCIMIENTO_PV.CONOCIMIENTO_ID);
            return View(CONOCIMIENTO_PV);
        }

        // GET: CONOCIMIENTO_PV/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CONOCIMIENTO_PV cONOCIMIENTO_PV = await db.CONOCIMIENTO_PV.FindAsync(id);
            if (cONOCIMIENTO_PV == null)
            {
                return HttpNotFound();
            }
            ViewBag.CONOCIMIENTO_ID = new SelectList(db.CONOCIMIENTO, "ID", "TIPO", cONOCIMIENTO_PV.CONOCIMIENTO_ID);
            return View(cONOCIMIENTO_PV);
        }

        // POST: CONOCIMIENTO_PV/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,DIRECCION,TIPO,CREATEDATE,CONOCIMIENTO_ID")] CONOCIMIENTO_PV cONOCIMIENTO_PV)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cONOCIMIENTO_PV).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.CONOCIMIENTO_ID = new SelectList(db.CONOCIMIENTO, "ID", "TIPO", cONOCIMIENTO_PV.CONOCIMIENTO_ID);
            return View(cONOCIMIENTO_PV);
        }

        // GET: CONOCIMIENTO_PV/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CONOCIMIENTO_PV cONOCIMIENTO_PV = await db.CONOCIMIENTO_PV.FindAsync(id);
            if (cONOCIMIENTO_PV == null)
            {
                return HttpNotFound();
            }
            return View(cONOCIMIENTO_PV);
        }

        // POST: CONOCIMIENTO_PV/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            CONOCIMIENTO_PV cONOCIMIENTO_PV = await db.CONOCIMIENTO_PV.FindAsync(id);
            db.CONOCIMIENTO_PV.Remove(cONOCIMIENTO_PV);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
