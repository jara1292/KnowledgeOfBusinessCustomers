﻿using Clientes.Logica;
using Clientes.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using static Clientes.Models.CONOCIMIENTO;

namespace Clientes.Controllers
{
    public class ClienteController : Controller
    {
        //instancia a modelo-contexto de base de datos
        private EmpresarialesModel db = new EmpresarialesModel();

        //logica para listar registros y agregar nuevos
        private ClienteLogic cl = new ClienteLogic();



        //public ActionResult ListaEjecutivos(string term)
        //{
        //    var consulta = db.COUSUARIOS.ToList();
        //    return Json(consulta, JsonRequestBehavior.AllowGet);
        //}



        //vista login
        public ActionResult Login()
        {
            Session.Abandon();
            return View();
        }


        //metodo para validar acceso a usuario por perfil
        [HttpPost]
        public ActionResult Login(string usuario, string password)
        {
            var query = (from c in db.COUSUARIOS where c.VENDEDOR == usuario && c.PASSWORD == password select c);

            if (query.Any())
            {
                Session["usuario"] = usuario;
                Session["Nombre"] = query.First().NOMBRE;
                Session["perfil"] = query.First().PERFIL;
                Session["idusuario"] = query.First().ID;
                Session["pwd"] = password;

                if (query.First().PERFIL == 1)
                {
                    var url = Request.Url.ToString();
                    if (url.Contains("201.144.210.148"))
                    {
                        return Redirect("/activaciones/Empresariales/Clientes/Cliente/Inicio");
                    }
                    return Redirect("/Empresariales/Clientes/Cliente/Inicio");
                }
                else if (query.First().PERFIL == 2)
                {
                    var url = Request.Url.ToString();
                    if (url.Contains("201.144.210.148"))
                    {
                        return Redirect("/activaciones/Empresariales/Clientes/Cliente/EJECUTIVO?usuario=" + query.First().ID);
                    }
                    return Redirect("/Empresariales/Clientes/Cliente/EJECUTIVO?usuario=" + query.First().ID);
                }
                else
                {
                    ViewBag.Error = "Usuario y/o contraseña incorrecto";
                    return View();
                }

            }
            else
            {
                ViewBag.Error = "Usuario y/o contraseña incorrecto";
                return View();
            }
        }



        //listado de registros de acuerdo a ejecutivo logueado
        public ActionResult EJECUTIVO()
        {
            if (Session["usuario"] == null)
            {
                var url = Request.Url.ToString();
                if (url.Contains("201.144.210.148"))
                {
                    return Redirect("/activaciones/Empresariales/Clientes/Cliente/Login");
                }
                return Redirect("/Empresariales/Clientes/Cliente/Login");
            }

            string idejecutivo = Request.QueryString["usuario"];
            var obtenerejec = db.COUSUARIOS.Find(Convert.ToInt32(idejecutivo));
            var query = from c in db.CONOCIMIENTO where c.EJECUTIVO_FONIX == obtenerejec.VENDEDOR select c;
            return View(query.ToList());
        }



        // Listar todos los clientes
        public ActionResult Index()
        {
            return View(cl.Listar());
        }

        //Vista con campo para busqueda por ejecutivo perfil administrador
        public ActionResult Inicio()
        {
            Session["Error"] = "";
            Session["mensaje"] = "";
            if (Session["usuario"] == null)
            {
                var url = Request.Url.ToString();
                if (url.Contains("201.144.210.148"))
                {
                    return Redirect("/activaciones/Empresariales/Clientes/Cliente/Login");
                }
                return Redirect("/Empresariales/Clientes/Cliente/Login");
            }

            return View(db.CONOCIMIENTO.ToList().OrderBy(x => x.RAZON_SOCIAL));
        }





        //resultado de busqueda por ejecutivo perfil administrador
        public ActionResult Busqueda(string vendedor)
        {
            if (Session["usuario"] == null)
            {
                var url = Request.Url.ToString();
                if (url.Contains("201.144.210.148"))
                {
                    return Redirect("/activaciones/Empresariales/Clientes/Cliente/Login");
                }
                return Redirect("/Empresariales/Clientes/Cliente/Login");
            }

            string ejecutivo = vendedor;
            if (!string.IsNullOrEmpty(vendedor))
            {
                var url = Request.Url.ToString();
                if (url.Contains("201.144.210.148"))
                {
                    return Redirect("/activaciones/Empresariales/Clientes/Cliente/Busqueda?id=" + vendedor);
                }
                return Redirect("/Empresariales/Clientes/Cliente/Busqueda?id=" + vendedor);
            }

            if (Request.QueryString["id"] != null)
            {
                ejecutivo = Request.QueryString["id"];
            }
            var query = from s in db.CONOCIMIENTO where s.EJECUTIVO_FONIX.Contains(ejecutivo) select s;

            return View(query.ToList());
        }


        //vista registrar nuevo cliente
        public ActionResult Registrar()
        {
            if (Session["usuario"] == null)
            {
                var url = Request.Url.ToString();
                if (url.Contains("201.144.210.148"))
                {
                    return Redirect("/activaciones/Empresariales/Clientes/Cliente/Login");
                }
                return Redirect("/Empresariales/Clientes/Cliente/Login");
            }


            var query = db.COUSUARIOS;
            var ejecutivos = new SelectList(query.ToList(), "VENDEDOR", "NOMBRE");
            ViewData["grupos"] = new SelectList(db.GRUPOS.ToList(), "ID", "RAZON");
            ViewData["ejecutivos"] = ejecutivos;

            var giros = new SelectList(new[]
                                          {
                                              new {ID="",Name="--SELECCIONE GIRO"},
                                              new {ID="SERVICIOS",Name="SERVICIOS"},
                                              new {ID="COMERCIO",Name="COMERCIO"},
                                              new {ID="INDUSTRIAL",Name="INDUSTRIAL"},
                                              new{ID="PARTICULAR",Name="PARTICULAR"},
                                          },
               "ID", "Name", 1);
            ViewData["giros"] = giros;
            return View(new ConocimientoViewModel());
        }


        //Post-metodo para registrar nuevo cliente
        [HttpPost]
        public ActionResult Registrar(ConocimientoViewModel model, string action)
        {

            var query = db.COUSUARIOS;
            var ejecutivos = new SelectList(query.ToList(), "VENDEDOR", "NOMBRE");


            var giros = new SelectList(new[]
                                         {
                                              new {ID="",Name="--SELECCIONE GIRO"},
                                              new {ID="SERVICIOS",Name="SERVICIOS"},
                                              new {ID="COMERCIO",Name="COMERCIO"},
                                              new {ID="INDUSTRIAL",Name="INDUSTRIAL"},
                                              new{ID="PARTICULAR",Name="PARTICULAR"},
                                          },
              "ID", "Name", 1);

            if (action == "generar")
            {
                if (!string.IsNullOrEmpty(model.RAZON_SOCIAL))
                {
                    if (cl.Registrar(model.ToModel()))
                    {
                        var maximo = db.CONOCIMIENTO.Max(x => x.ID);
                        if (!string.IsNullOrEmpty(model.CALLE_No))
                        {
                            var direccion = new DIRECCION();
                            direccion.CALLE_No = model.CALLE_No;
                            direccion.COLONIA = model.COLONIA;
                            direccion.MUNICIPIO = model.MUNICIPIO;
                            direccion.ESTADO = model.ESTADO;
                            direccion.CP = model.CP;
                            direccion.CONOCIMIENTO_ID = maximo;
                            direccion.RFC = model.RFC;
                            direccion.TIPO = "FISCAL";
                            db.DIRECCION.Add(direccion);
                            db.SaveChanges();
                        }


                        if (!string.IsNullOrEmpty(model.CALLE_No2))
                        {
                            var direccion2 = new DIRECCION();
                            direccion2.CALLE_No = model.CALLE_No2;
                            direccion2.COLONIA = model.COLONIA2;
                            direccion2.MUNICIPIO = model.MUNICIPIO2;
                            direccion2.ESTADO = model.ESTADO2;
                            direccion2.CP = model.CP2;
                            direccion2.CONOCIMIENTO_ID = maximo;
                            direccion2.RFC = model.RFC;
                            direccion2.TIPO = "ENTREGA";
                            db.DIRECCION.Add(direccion2);
                            db.SaveChanges();
                        }
                        
                        Session["mensaje"] = "Se ha registrado correctamente el cliente : " + model.NOMBRE_COMERCIAL;
                        return Redirect("Registrar");
                    }
                }
                else
                {
                    ModelState.AddModelError("cliente", "Debe agregar un cliente para el comprobante");
                }
            }
            else if (action == "agregar_producto")
            {
                //{
                //    // Si no ha pasado nuestra validación, mostramos el mensaje personalizado de error
                //    if (!model.SeAgregoUnProductoValido())
                //    {
                //        ModelState.AddModelError("producto_agregar", "Solo puede agregar un producto válido al detalle");
                //    }
                //    else if (model.ExisteEnDetalle(model.CabeceraProductoId))
                //    {
                //        ModelState.AddModelError("producto_agregar", "El producto elegido ya existe en el detalle");
                //    }
                //    else
                //    {
                ViewData["grupos"] = new SelectList(db.GRUPOS.ToList(), "ID", "RAZON");
                ViewData["giros"] = giros;
                //ViewData["tipos"] = tipos;
                ViewData["ejecutivos"] = ejecutivos;
                model.AgregarItemADetalle();
                return View(model);
                //}


            }
            else if (action == "retirar_producto")
            {
                ViewData["grupos"] = new SelectList(db.GRUPOS.ToList(), "ID", "RAZON");
                ViewData["giros"] = giros;
                //ViewData["tipos"] = tipos;
                ViewData["ejecutivos"] = ejecutivos;
                model.RetirarItemDeDetalle();
            }
            else
            {
                throw new Exception("Acción no definida ..");
            }
            ViewData["grupos"] = new SelectList(db.GRUPOS.ToList(), "ID", "RAZON");
            ViewData["giros"] = giros;
            //ViewData["tipos"] = tipos;
            ViewData["ejecutivos"] = ejecutivos;
            return View(model);
        }









        //get vista detalles-editar cliente
        public async Task<ActionResult> Detalles(int? id)
        {
            if (Session["usuario"] == null)
            {
                var url = Request.Url.ToString();
                if (url.Contains("201.144.210.148"))
                {
                    return Redirect("/activaciones/Empresariales/Clientes/Cliente/Login");
                }
                return Redirect("/Empresariales/Clientes/Cliente/Login");
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CONOCIMIENTO view = await db.CONOCIMIENTO.FindAsync(id);
            if (view == null)
            {
                return HttpNotFound();
            }

            ConocimientoViewModel registro = new ConocimientoViewModel();


            //ALIADO
            if (view.CONTACTO_ALIADO != null)
            {
                string[] contactoAliado = view.CONTACTO_ALIADO.ToString().Split('/');
                if (contactoAliado.Length > 1)
                {
                    registro.CONTACTO_ALIADO = contactoAliado[0];
                    registro.CONTACTO_ALIADO_EXT = contactoAliado[1];
                    if (contactoAliado.Length > 2)
                    {
                        registro.CONTACTO_ALIADO_CORREO = contactoAliado[2];
                    }
                }
                else { registro.CONTACTO_ALIADO = view.CONTACTO_ALIADO; }
            }


            //TECNICO
            if (view.CONTACTO_TECNICO != null)
            {
                string[] contactoTecnico = view.CONTACTO_TECNICO.ToString().Split('/');
                if (contactoTecnico.Length > 1)
                {
                    registro.CONTACTO_TECNICO = contactoTecnico[0];
                    registro.CONTACTO_TECNICO_EXT = contactoTecnico[1];
                    if (contactoTecnico.Length > 2)
                    {
                        registro.CONTACTO_TECNICO_CORREO = contactoTecnico[2];
                    }
                }
                else { registro.CONTACTO_TECNICO = view.CONTACTO_TECNICO; }
            }


            //PRINCIPAL
            if (view.CONTACTO_PRINCIPAL != null)
            {
                string[] contactoPrincipal = view.CONTACTO_PRINCIPAL.ToString().Split('/');
                if (contactoPrincipal.Length > 1)
                {
                    registro.CONTACTO_PRINCIPAL = contactoPrincipal[0];
                    registro.CONTACTO_PRINCIPAL_EXT = contactoPrincipal[1];
                    if (contactoPrincipal.Length > 2)
                    {
                        registro.CONTACTO_PRINCIPAL_CORREO = contactoPrincipal[2];
                    }
                }
                else { registro.CONTACTO_PRINCIPAL = view.CONTACTO_PRINCIPAL; }
            }



            //CXP
            if (view.CONTACTO_CXP != null)
            {
                string[] contactoCXP = view.CONTACTO_CXP.ToString().Split('/');
                if (contactoCXP.Length > 1)
                {
                    registro.CONTACTO_CXP = contactoCXP[0];
                    registro.CONTACTO_CXP_EXT = contactoCXP[1];
                    if (contactoCXP.Length > 2)
                    {
                        registro.CONTACTO_CXP_CORREO = contactoCXP[2];
                    }
                }
                else { registro.CONTACTO_CXP = view.CONTACTO_CXP; }
            }
            

            registro.ANIOS_MERCADO = view.ANIOS_MERCADO;
            registro.CARGO_ALIADO = view.CARGO_ALIADO;
            registro.CARGO_CXP = view.CARGO_CXP;
            registro.CARGO_PRINCIPAL = view.CARGO_PRINCIPAL;
            registro.CARGO_TECNICO = view.CARGO_TECNICO;
            registro.COMPETENCIA = view.COMPETENCIA;
            registro.DESC_ACTIVIDAD = view.DESC_ACTIVIDAD;
            registro.GIRO = view.GIRO;
            registro.SUBTIPO_GIRO = view.SUBTIPO_GIRO;
            //registro.DIRECCION = view.DIRECCION;
            registro.EJECUTIVO_FONIX = view.EJECUTIVO_FONIX;
            //registro.FECHA_ACTUALIZA = view.FECHA_ACTUALIZA;
            registro.NOMBRE_COMERCIAL = view.NOMBRE_COMERCIAL;
            registro.NO_EMPLEADOS = view.NO_EMPLEADOS;
            registro.PAGINA = view.PAGINA;
            registro.PRINCIPALES_CLIENTES = view.PRINCIPALES_CLIENTES;
            registro.RAZON_SOCIAL = view.RAZON_SOCIAL;
            registro.REP_LEGAL = view.REP_LEGAL;
            registro.RFC = view.RFC;
            registro.TEL_EMPRESA = view.TEL_EMPRESA;
            //registro.TIPO = view.TIPO;
            registro.CLAVE_NISI = view.CLAVE_NISI;
            registro.CLAVE_PORTAL = view.CLAVE_PORTAL;
            registro.USUARIO_PORTAL = view.USUARIO_PORTAL;
            registro.ALIAS_PORTAL = view.ALIAS_PORTAL;
            registro.CLAVE_INTERBANCARIA = view.CLAVE_INTERBANCARIA;
            registro.REFERENCIA_RAP = view.REFERENCIA_RAP;
            registro.ESPORADICO = view.ESPORADICO;
            registro.MERCADO = view.MERCADO;
            registro.FOLIO_VERIFICACION = view.FOLIO_VERIFICACION;
            registro.PROVEEDORES = view.PROVEEDORES;
            registro.GRUPO_ID = view.GRUPO_ID;


            var direccion = db.DIRECCION.Where(x => x.CONOCIMIENTO_ID == id && x.TIPO=="FISCAL").FirstOrDefault();
            var direccion2 = db.DIRECCION.Where(x => x.CONOCIMIENTO_ID == id && x.TIPO == "ENTREGA").FirstOrDefault();

            if (direccion != null)
            {
                registro.CALLE_No = direccion.CALLE_No;
                registro.COLONIA = direccion.COLONIA;
                registro.MUNICIPIO = direccion.MUNICIPIO;
                registro.ESTADO = direccion.ESTADO;
                registro.CP = direccion.CP;
            }

            if (direccion2 != null)
            {
                registro.CALLE_No2 = direccion2.CALLE_No;
                registro.COLONIA2 = direccion2.COLONIA;
                registro.MUNICIPIO2 = direccion2.MUNICIPIO;
                registro.ESTADO2 = direccion2.ESTADO;
                registro.CP2 = direccion2.CP;
            }



            var longitud = view.CONOCIMIENTO_PV.Count;

            List<CONOCIMIENTO_PVViewModel> lista = new List<CONOCIMIENTO_PVViewModel>();

            foreach (var i in view.CONOCIMIENTO_PV)
            {
                //var l = view.CONOCIMIENTO_PV.IndexOf(i) - 1;
                registro.CONOCIMIENTO_PV.Add(new CONOCIMIENTO_PVViewModel
                {
                    TIPO = i.TIPO,
                    DIRECCION = i.DIRECCION

                });

            }

            var comprobar_situacion = from s in db.SITUACION where s.IDCONOCIMIENTO == id select s;
            ViewBag.Situacion = "N";
            if (comprobar_situacion.ToList().Count > 0)
            {
                ViewBag.Situacion = "S";
                ViewBag.idsit = comprobar_situacion.FirstOrDefault().ID;
            }

            ViewData["grupos"] = new SelectList(db.GRUPOS.ToList(), "ID", "RAZON");
            var query = db.COUSUARIOS;
            var ejecutivos = new SelectList(query.ToList(), "VENDEDOR", "NOMBRE");
            ViewData["ejecutivos"] = ejecutivos;

            //var tipos = new SelectList(new[]
            //                              {
            //                                  new {ID="",Name="--SELECCIONE EL TIPO DE PERSONA"},
            //                                  new {ID="NUEVO",Name="NUEVO CLIENTE"},
            //                                  new{ID="PROSPECTO",Name="PROSPECTO"},
            //                              },
            //   "ID", "Name", 1);
            //ViewData["tipos"] = tipos;

            var giros = new SelectList(new[]
                                          {
                                              new {ID="",Name="--SELECCIONE GIRO"},
                                              new {ID="SERVICIOS",Name="SERVICIOS"},
                                              new {ID="COMERCIO",Name="COMERCIO"},
                                              new {ID="INDUSTRIAL",Name="INDUSTRIAL"},
                                              new{ID="PARTICULAR",Name="PARTICULAR"},
                                          },
               "ID", "Name", 1);
            ViewData["giros"] = giros;
            var estatusc = new SelectList(new[]
                                          {
                                              new {ID="",Name="--SELECCIONE OPCION"},
                                              new {ID="AF",Name="ACTIVO FÓNIX"},
                                              new{ID="ATD",Name="ACTIVO TD"},
                                              new{ID="ADF",Name="ACTIVO DAF"},
                                              new{ID="PMVT",Name="PORT OUT MOVISTAR"},
                                              new{ID="PATT",Name="PORT OUT AT&T"},
                                              new{ID="CDD",Name="CAMBIO DE DISTRIBUIDOR"},
                                          },
               "ID", "Name", 1);
            ViewData["estatusc"] = estatusc;
            ViewBag.identificador = view.ID;
            return View(registro);
        }



        //post-metodo para guardar edicion de detalles de registro conocimiento 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Detalles(ConocimientoViewModel view)
        {
            var url = Request.Url.ToString();
            var queryej = db.COUSUARIOS;
            var ejecutivos = new SelectList(queryej.ToList(), "VENDEDOR", "NOMBRE");
            ViewData["ejecutivos"] = ejecutivos;
            ViewData["grupos"] = new SelectList(db.GRUPOS.ToList(), "ID", "RAZON");
            var estatusc = new SelectList(new[]
                                          {
                                              new {ID="",Name="--SELECCIONE OPCION"},
                                              new {ID="AF",Name="ACTIVO FÓNIX"},
                                              new{ID="ATD",Name="ACTIVO TD"},
                                              new{ID="ADF",Name="ACTIVO DAF"},
                                              new{ID="PMVT",Name="PORT OUT MOVISTAR"},
                                              new{ID="PATT",Name="PORT OUT AT&T"},
                                              new{ID="CDD",Name="CAMBIO DE DISTRIBUIDOR"},
                                          },
               "ID", "Name", 1);
            ViewData["estatusc"] = estatusc;
            //var tipos = new SelectList(new[]
            //                              {
            //                                  new {ID="",Name="--SELECCIONE EL TIPO DE PERSONA"},
            //                                  new {ID="NUEVO",Name="NUEVO CLIENTE"},
            //                                  new{ID="PROSPECTO",Name="PROSPECTO"},
            //                              },
            //   "ID", "Name", 1);
            //ViewData["tipos"] = tipos;

            var giros = new SelectList(new[]
                                          {
                                              new {ID="",Name="--SELECCIONE GIRO"},
                                              new {ID="SERVICIOS",Name="SERVICIOS"},
                                              new {ID="COMERCIO",Name="COMERCIO"},
                                              new {ID="INDUSTRIAL",Name="INDUSTRIAL"},
                                              new{ID="PARTICULAR",Name="PARTICULAR"},
                                          },
               "ID", "Name", 1);
            ViewData["giros"] = giros;
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var query = db.CONOCIMIENTO.Find(view.ID);
                    query.ANIOS_MERCADO = view.ANIOS_MERCADO;
                    query.CARGO_ALIADO = view.CARGO_ALIADO;
                    query.CARGO_CXP = view.CARGO_CXP;
                    query.CARGO_PRINCIPAL = view.CARGO_PRINCIPAL;
                    query.CARGO_TECNICO = view.CARGO_TECNICO;
                    query.COMPETENCIA = view.COMPETENCIA;
                    query.CONTACTO_ALIADO = view.CONTACTO_ALIADO + "/" + view.CONTACTO_ALIADO_EXT + "/" + view.CONTACTO_ALIADO_CORREO;
                    query.CONTACTO_CXP = view.CONTACTO_CXP + "/" + view.CONTACTO_CXP_EXT + "/" + view.CONTACTO_CXP_CORREO;
                    query.CONTACTO_PRINCIPAL = view.CONTACTO_PRINCIPAL + "/" + view.CONTACTO_PRINCIPAL_EXT + "/" + view.CONTACTO_PRINCIPAL_CORREO;
                    query.CONTACTO_TECNICO = view.CONTACTO_TECNICO + "/" + view.CONTACTO_TECNICO_EXT + "/" + view.CONTACTO_TECNICO_CORREO;
                    query.DESC_ACTIVIDAD = view.DESC_ACTIVIDAD;
                    query.GIRO = view.GIRO;
                    query.SUBTIPO_GIRO = view.SUBTIPO_GIRO;
                    //query.DIRECCION = view.DIRECCION;
                    query.EJECUTIVO_FONIX = view.EJECUTIVO_FONIX;
                    query.NOMBRE_COMERCIAL = view.NOMBRE_COMERCIAL;
                    query.NO_EMPLEADOS = view.NO_EMPLEADOS;
                    query.PAGINA = view.PAGINA;
                    query.PRINCIPALES_CLIENTES = view.PRINCIPALES_CLIENTES;
                    query.RAZON_SOCIAL = view.RAZON_SOCIAL;
                    query.REP_LEGAL = view.REP_LEGAL;
                    query.RFC = view.RFC;
                    query.TEL_EMPRESA = view.TEL_EMPRESA;
                    //query.TIPO = view.TIPO;
                    query.FECHA_ACTUALIZA = view.FECHA_ACTUALIZA;
                    query.CLAVE_NISI = view.CLAVE_NISI;
                    query.CLAVE_PORTAL = view.CLAVE_PORTAL;
                    query.USUARIO_PORTAL = view.USUARIO_PORTAL;
                    query.ALIAS_PORTAL = view.ALIAS_PORTAL;
                    query.CLAVE_INTERBANCARIA = view.CLAVE_INTERBANCARIA;
                    query.REFERENCIA_RAP = view.REFERENCIA_RAP;
                    query.ESPORADICO = view.ESPORADICO;
                    query.MERCADO = view.MERCADO;
                    query.FOLIO_VERIFICACION = view.FOLIO_VERIFICACION;
                    query.PROVEEDORES = view.PROVEEDORES;
                    query.GRUPO_ID = view.GRUPO_ID;
                    db.SaveChanges();




                    var direccion = db.DIRECCION.Where(x => x.CONOCIMIENTO_ID == view.ID && x.TIPO == "FISCAL").FirstOrDefault();

                    if (direccion != null)
                    {
                        direccion.CALLE_No = view.CALLE_No;
                        direccion.COLONIA = view.COLONIA;
                        direccion.MUNICIPIO = view.MUNICIPIO;
                        direccion.CP = view.CP;
                        direccion.RFC = view.RFC;
                        direccion.ESTADO = view.ESTADO;
                        direccion.CONOCIMIENTO_ID = view.ID;
                        db.SaveChanges();
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(view.CALLE_No))
                        {
                            DIRECCION objetodir = new DIRECCION();
                            objetodir.CALLE_No = view.CALLE_No;
                            objetodir.COLONIA = view.COLONIA;
                            objetodir.MUNICIPIO = view.MUNICIPIO;
                            objetodir.CP = view.CP;
                            objetodir.RFC = view.RFC;
                            objetodir.ESTADO = view.ESTADO;
                            objetodir.CONOCIMIENTO_ID = view.ID;
                            objetodir.TIPO = "FISCAL";
                            db.DIRECCION.Add(objetodir);
                            db.SaveChanges();
                        }
                    }





                    var direccion2 = db.DIRECCION.Where(x => x.CONOCIMIENTO_ID == view.ID && x.TIPO == "ENTREGA").FirstOrDefault();

                    if (direccion2 != null)
                    {
                        direccion2.CALLE_No = view.CALLE_No2;
                        direccion2.COLONIA = view.COLONIA2;
                        direccion2.MUNICIPIO = view.MUNICIPIO2;
                        direccion2.CP = view.CP2;
                        direccion2.RFC = view.RFC;
                        direccion2.ESTADO = view.ESTADO2;
                        direccion2.CONOCIMIENTO_ID = view.ID;
                        db.SaveChanges();
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(view.CALLE_No2))
                        {
                            DIRECCION objetodir = new DIRECCION();
                            objetodir.CALLE_No = view.CALLE_No2;
                            objetodir.COLONIA = view.COLONIA2;
                            objetodir.MUNICIPIO = view.MUNICIPIO2;
                            objetodir.CP = view.CP2;
                            objetodir.RFC = view.RFC;
                            objetodir.ESTADO = view.ESTADO2;
                            objetodir.CONOCIMIENTO_ID = view.ID;
                            objetodir.TIPO = "ENTREGA";

                            db.DIRECCION.Add(objetodir);
                            db.SaveChanges();
                        }
                        
                    }

                    var update_estatus2 = db.Database.ExecuteSqlCommand("UPDATE EMPRESARIALES.ADENDA SET RAZON_SOCIAL=@razon WHERE RFC=@rfc", new SqlParameter("@razon", view.RAZON_SOCIAL), new SqlParameter("@rfc", view.RFC));
                    if (! string.IsNullOrEmpty(view.ESTATUS_CUENTA))
                    {
                        var update_estatus = db.Database.ExecuteSqlCommand("UPDATE EMPRESARIALES.ADENDA SET ESTATUS_CUENTA=@ecuenta WHERE RFC=@rfc", new SqlParameter("@ecuenta", view.ESTATUS_CUENTA), new SqlParameter("@rfc", view.RFC));
                    }
                    
                    var delete = db.Database.ExecuteSqlCommand("DELETE FROM EMPRESARIALES.CONOCIMIENTO_PV WHERE CONOCIMIENTO_ID=@cid", new SqlParameter("@cid", view.ID));

                    //var 
                    //SqlParameterCollection sp = null;
                    //sp.Add(new SqlParameter("@ejecutivo", System.Data.SqlDbType.NVarChar)).Value = view.EJECUTIVO_FONIX;
                    //sp.Add(new SqlParameter("@razon", System.Data.SqlDbType.NVarChar)).Value = view.RAZON_SOCIAL;

                    var update = db.Database.ExecuteSqlCommand("UPDATE EMPRESARIALES.ADENDA SET EJECUTIVO=@ejecutivo WHERE RFC=@razon", new SqlParameter("@ejecutivo", view.EJECUTIVO_FONIX), new SqlParameter("@razon", view.RFC));

                    foreach (var d in view.CONOCIMIENTO_PV)
                    {
                        CONOCIMIENTO_PV objetopv = new CONOCIMIENTO_PV();
                        objetopv.TIPO = d.TIPO;
                        objetopv.DIRECCION = d.DIRECCION;
                        objetopv.CONOCIMIENTO_ID = view.ID;
                        db.CONOCIMIENTO_PV.Add(objetopv);
                        db.SaveChanges();

                    }


                    Session["Error"] = "";
                    Session["mensaje"] = "";

                    transaction.Commit();

                    

                    if (Convert.ToInt16(Session["perfil"]) == 2)
                    {
                        if (url.Contains("201.144.210.148"))
                        {
                            return Redirect("/activaciones/Empresariales/Clientes/Cliente/EJECUTIVO?usuario=" + Session["idusuario"]);
                        }
                        return Redirect("/Empresariales/Clientes/Cliente/EJECUTIVO?usuario=" + Session["idusuario"]);
                    }

                   
                    if (url.Contains("201.144.210.148"))
                    {
                        return Redirect("/activaciones/Empresariales/Clientes/Cliente/Inicio");
                    }

                    return Redirect("/Empresariales/Clientes/Cliente/Inicio");

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    ViewBag.Error = "ERROR:" + ex.InnerException + "::" + ex.StackTrace;
                }
            }
            Session["error"] = ViewBag.Error;

            if (url.Contains("201.144.210.148"))
            {
                return Redirect("/activaciones/Empresariales/Clientes/Cliente/Detalles/" + view.ID);
            }
            return Redirect("/Empresariales/Clientes/Cliente/Detalles/" + view.ID);
        }




        public ActionResult Cliente(int? id)
        {
            CONOCIMIENTO cliente = db.CONOCIMIENTO.Find(id);

            ViewData["fiscal"] = "";
            ViewData["entrega"] = "";

            var fiscal = db.DIRECCION.Where(x => x.CONOCIMIENTO_ID == id && x.TIPO == "FISCAL").FirstOrDefault();
            var entrega = db.DIRECCION.Where(x => x.CONOCIMIENTO_ID == id && x.TIPO == "ENTREGA").FirstOrDefault();

            if (fiscal!=null)
            {
                ViewData["fiscal"] = "Calle: " + fiscal.CALLE_No + ", Colonia: " + fiscal.COLONIA + ", Municipio: " + fiscal.ESTADO + ", CP: " + fiscal.CP;
            }
            if (entrega != null)
            {
                ViewData["entrega"] = "Calle: " + entrega.CALLE_No + ", Colonia: " + entrega.COLONIA + ", Municipio: " + entrega.ESTADO + ", CP: " + entrega.CP;
            }
            


            return View(cliente);
        }




        //metodo para abandonar-destruir la sesion
        public ActionResult Salir()
        {
            Session.Abandon();
            var url = Request.Url.ToString();
            if (url.Contains("201.144.210.148"))
            {
                return Redirect("/activaciones/Empresariales/Clientes/Cliente/Login");
            }
            return Redirect("/Empresariales/Clientes/Cliente/Login");
        }





        //public ActionResult Indice(string sortOrder, string SearchString)
        //{
        //    ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
        //    ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
        //    var regs = from s in db.CONOCIMIENTO
        //                   select s;


        //    if (!String.IsNullOrEmpty(SearchString))
        //    {
        //        regs = regs.Where(s => s.EJECUTIVO_FONIX.Contains(SearchString)
        //                               || s.NOMBRE_COMERCIAL.Contains(SearchString));
        //    }

        //    switch (sortOrder)
        //    {
        //        case "razon":
        //            regs = regs.OrderBy(s => s.RAZON_SOCIAL);
        //            break;
        //        case "nombre":
        //            regs = regs.OrderBy(s => s.NOMBRE_COMERCIAL);
        //            break;
        //        case "ejecutivo":
        //            regs = regs.OrderBy(s => s.EJECUTIVO_FONIX);
        //            break;
        //        case "fecha":
        //            regs = regs.OrderBy(s => s.FECHA_ACTUALIZA);
        //            break;
        //        //case "date_desc":
        //        //    students = students.OrderByDescending(s => s.FECHA_ACTUALIZA);
        //        //    break;
        //        default:
        //            regs = regs.OrderBy(s => s.EJECUTIVO_FONIX);
        //            break;
        //    }
        //    return View(regs.ToList());
        //}
    }
}