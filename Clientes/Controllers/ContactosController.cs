﻿using Clientes.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Clientes.Controllers
{
    public class ContactosController : Controller
    {
        private EmpresarialesModel db = new EmpresarialesModel();
        // GET: Contactos
        //[OutputCache(Duration = 300, VaryByParam = "none")] //cached for 300 seconds 
        public ActionResult Index()
        {
            return View(db.CONTACTOes.ToList());
        }



        //Calendario
        //[OutputCache(Duration = 300, VaryByParam = "none")] //cached for 300 seconds 
        public ActionResult Inicio(string id)
        {
            if (!String.IsNullOrEmpty(id))
            {
                ViewBag.vend = id;
            }

            ViewData["ejecutivos"] = new SelectList(db.COUSUARIOS.ToList().Where(x => x.ACTIVO = true && x.PERFIL != 1), "VENDEDOR", "NOMBRE");
            return View();
        }

        public JsonResult Eventos(string id,DateTime start, DateTime end)
        {
            //var actividades = db.CONTACTOes.Where(x => x.FECHA_COMPROMISO != null && (x.FECHA_COMPROMISO >= start && x.FECHA_COMPROMISO <= end)).ToList();
            var compromisos = (from cn in db.CONOCIMIENTO
                               join c in db.CONTACTOes
                               on cn.ID equals c.CONOCIMIENTO_ID
                               join cp in db.COMPROMISOes
                               on c.ID equals cp.CONTACTO_ID
                               select new
                               {
                                   cp.ID,
                                   cp.FECHA_COMPROMISO,
                                   cn.NOMBRE_COMERCIAL,
                                   cp.COMPROMISO1,
                                   cn.EJECUTIVO_FONIX,
                               }).ToList();


            var proximo = (from cn in db.CONOCIMIENTO
                               join c in db.CONTACTOes
                               on cn.ID equals c.CONOCIMIENTO_ID
                               join cp in db.PROXIMO_CONTACTO
                               on c.ID equals cp.CONTACTO_ID
                               select new
                               {
                                   cp.ID,
                                   cp.MOTIVO_PROXIMO_CONTACTO,
                                   cn.NOMBRE_COMERCIAL,
                                   cp.FECHA_PROXIMO_CONTACTO,
                                   cp.HORA_PROXIMO_CONTACTO,
                                   cn.EJECUTIVO_FONIX
                               }).ToList();


            if (!String.IsNullOrEmpty(id))
            {
                compromisos = compromisos.Where(x => x.EJECUTIVO_FONIX == id).ToList();
                proximo = proximo.Where(x => x.EJECUTIVO_FONIX == id).ToList();
            }

            List<Events> eventos = new List<Events>();
            foreach (var item in compromisos)
            {
                Events evento = new Events();
                evento.id = item.ID;
                evento.start = Convert.ToDateTime(item.FECHA_COMPROMISO).ToString("s", System.Globalization.CultureInfo.InvariantCulture);

                var query = db.COMPROMISOes.Find(item.ID);
                var contacto = db.CONTACTOes.Find(query.CONTACTO_ID);
                var max = db.CONTACTOes.Where(x => x.CONOCIMIENTO_ID == contacto.CONOCIMIENTO_ID).Max(x=>x.FECHA_CONTACTO);

                evento.estatus = max == item.FECHA_COMPROMISO ? "Cumplido" : item.FECHA_COMPROMISO > max && item.FECHA_COMPROMISO > DateTime.Now ? "Vigente" : "Incumplido";

                evento.color = "#7f181b";
                //evento.end = item.FECHA_COMPROMISO.ToString("o");
                //evento.title = item.Nombre + " " + item.Descripcion;
                evento.title = "Compromiso-" + item.NOMBRE_COMERCIAL + "-" + item.COMPROMISO1 + "::" + evento.estatus;
                evento.ejecutivo = item.EJECUTIVO_FONIX;
                evento.tipo = "C";
                eventos.Add(evento);
            }

            foreach (var item in proximo)
            {
                Events evento = new Events();
                evento.id = item.ID;
                evento.start = Convert.ToDateTime(item.FECHA_PROXIMO_CONTACTO).ToString("s", System.Globalization.CultureInfo.InvariantCulture);


                var query = db.PROXIMO_CONTACTO.Find(item.ID);
                var contacto = db.CONTACTOes.Find(query.CONTACTO_ID);
                var max = db.CONTACTOes.Where(x => x.CONOCIMIENTO_ID == contacto.CONOCIMIENTO_ID).Max(x => x.FECHA_CONTACTO);


                evento.estatus = Convert.ToDateTime(max).ToShortDateString() == Convert.ToDateTime(item.FECHA_PROXIMO_CONTACTO).ToShortDateString() ? "Cumplido" : (item.FECHA_PROXIMO_CONTACTO > max  && item.FECHA_PROXIMO_CONTACTO > DateTime.Now) || (Convert.ToDateTime(item.FECHA_PROXIMO_CONTACTO).ToShortDateString() == DateTime.Now.ToShortDateString())  ? "Vigente" : "Incumplido";

                evento.color = "#0099e5";
                evento.title = "ProximoContacto-" + item.NOMBRE_COMERCIAL + "-" + item.MOTIVO_PROXIMO_CONTACTO + "::" + evento.estatus;
                evento.ejecutivo = item.EJECUTIVO_FONIX;
                evento.tipo = "P";
                eventos.Add(evento);
            }

            return Json(eventos, JsonRequestBehavior.AllowGet);
        }







        //[OutputCache(Duration = 300, VaryByParam = "none")] //cached for 300 seconds 
        public ActionResult Detail(int? id)
        {
            //return View(db.CONTACTOes.Find(id));
            ViewBag.CL = id;
            var cliente = db.CONOCIMIENTO.Find(id);
            ViewBag.Cliente = cliente.RAZON_SOCIAL;
            var query = (from c in db.CONTACTOes
                         //join p in db.PROXIMO_CONTACTO
                         //on c.ID equals p.CONTACTO_ID
                         join cp in db.COMPROMISOes
                         on c.ID equals cp.CONTACTO_ID
                         where c.CONOCIMIENTO_ID == id
                         select new
                         {
                             c.COMENTARIO,
                             c.CONOCIMIENTO_ID,
                             c.FECHA_CONTACTO,
                             c.ID,
                             c.NOMBRE_CONTACTO,
                             c.TIPO_CONTACTO,
                             //p.MOTIVO_PROXIMO_CONTACTO,
                             //p.FECHA_PROXIMO_CONTACTO,
                             //p.HORA_PROXIMO_CONTACTO,
                             cp.COMPROMISO1,
                             cp.FECHA_COMPROMISO
                         }).ToList();


            var query2 = (from c in db.CONTACTOes
                          join p in db.PROXIMO_CONTACTO
                          on c.ID equals p.CONTACTO_ID
                         //join cp in db.COMPROMISOes
                         //on c.ID equals cp.CONTACTO_ID
                         where c.CONOCIMIENTO_ID == id
                         select new
                         {
                             c.COMENTARIO,
                             c.CONOCIMIENTO_ID,
                             c.FECHA_CONTACTO,
                             c.ID,
                             c.NOMBRE_CONTACTO,
                             c.TIPO_CONTACTO,
                             p.MOTIVO_PROXIMO_CONTACTO,
                             p.FECHA_PROXIMO_CONTACTO,
                             p.HORA_PROXIMO_CONTACTO,
                             //cp.COMPROMISO1,
                             //cp.FECHA_COMPROMISO
                         }).ToList();


            List<ContactoModelView> contactos = new List<ContactoModelView>();
            foreach (var i in query)
            {
                ContactoModelView cont = new ContactoModelView();
                cont.COMENTARIO = i.COMENTARIO;
                //cont.MOTIVO_PROXIMO_CONTACTO = i.MOTIVO_PROXIMO_CONTACTO;
                cont.CONOCIMIENTO_ID = i.CONOCIMIENTO_ID;
                //cont.FECHA_PROXIMO_CONTACTO = i.FECHA_PROXIMO_CONTACTO;
                //cont.HORA_PROXIMO_CONTACTO = i.HORA_PROXIMO_CONTACTO;
                cont.FECHA_CONTACTO = i.FECHA_CONTACTO;
                cont.NOMBRE_CONTACTO = i.NOMBRE_CONTACTO;
                cont.TIPO_CONTACTO = i.TIPO_CONTACTO;
                cont.COMPROMISO = i.COMPROMISO1;
                cont.FECHA_COMPROMISO = i.FECHA_COMPROMISO;
                contactos.Add(cont);
            }


            foreach (var i in query2)
            {
                ContactoModelView cont = new ContactoModelView();
                cont.COMENTARIO = i.COMENTARIO;
                cont.MOTIVO_PROXIMO_CONTACTO = i.MOTIVO_PROXIMO_CONTACTO;
                cont.CONOCIMIENTO_ID = i.CONOCIMIENTO_ID;
                cont.FECHA_PROXIMO_CONTACTO = Convert.ToDateTime(i.FECHA_PROXIMO_CONTACTO).ToShortTimeString();
                cont.HORA_PROXIMO_CONTACTO = i.HORA_PROXIMO_CONTACTO;
                cont.FECHA_CONTACTO = i.FECHA_CONTACTO;
                cont.NOMBRE_CONTACTO = i.NOMBRE_CONTACTO;
                cont.TIPO_CONTACTO = i.TIPO_CONTACTO;
                //cont.COMPROMISO = i.COMPROMISO1;
                //cont.FECHA_COMPROMISO = i.FECHA_COMPROMISO;
                contactos.Add(cont);
            }


            return View(contactos);
        }

        //[OutputCache(Duration = 300, VaryByParam = "none")] //cached for 300 seconds 
        public ActionResult Compromisos(int id)
        {
            var cliente = db.CONOCIMIENTO.Find(id);
            ViewBag.Cliente = cliente.RAZON_SOCIAL;
            var query = (from c in db.CONTACTOes
                        join p in db.COMPROMISOes
                        on c.ID equals p.CONTACTO_ID
                        where c.CONOCIMIENTO_ID==id
                        select new {
                            c.COMENTARIO,
                            c.CONOCIMIENTO_ID,
                            c.FECHA_CONTACTO,
                            c.ID,
                            c.NOMBRE_CONTACTO,
                            c.TIPO_CONTACTO,
                            p.COMPROMISO1,
                            p.FECHA_COMPROMISO
                        }).ToList();
            List<ContactoModelView> contactos = new List<ContactoModelView>();
            foreach(var i in query)
            {
                ContactoModelView cont = new ContactoModelView();
                cont.COMENTARIO = i.COMENTARIO;
                cont.COMPROMISO = i.COMPROMISO1;
                cont.CONOCIMIENTO_ID = i.CONOCIMIENTO_ID;
                cont.FECHA_COMPROMISO = i.FECHA_COMPROMISO;
                cont.FECHA_CONTACTO = i.FECHA_CONTACTO;
                cont.NOMBRE_CONTACTO = i.NOMBRE_CONTACTO;
                cont.TIPO_CONTACTO = i.TIPO_CONTACTO;
                contactos.Add(cont);
            }
            return View(contactos);
        }


        //[OutputCache(Duration = 300, VaryByParam = "none")] //cached for 300 seconds 
        public ActionResult Proximo(int id)
        {
            var cliente = db.CONOCIMIENTO.Find(id);
            ViewBag.Cliente = cliente.RAZON_SOCIAL;
            var query = (from c in db.CONTACTOes
                         join p in db.PROXIMO_CONTACTO
                         on c.ID equals p.CONTACTO_ID
                         where c.CONOCIMIENTO_ID == id
                         select new
                         {
                             c.COMENTARIO,
                             c.CONOCIMIENTO_ID,
                             c.FECHA_CONTACTO,
                             c.ID,
                             c.NOMBRE_CONTACTO,
                             c.TIPO_CONTACTO,
                             p.MOTIVO_PROXIMO_CONTACTO,
                             p.FECHA_PROXIMO_CONTACTO,
                             p.HORA_PROXIMO_CONTACTO
                         }).ToList();
            List<ContactoModelView> contactos = new List<ContactoModelView>();
            foreach (var i in query)
            {
                ContactoModelView cont = new ContactoModelView();
                cont.COMENTARIO = i.COMENTARIO;
                cont.MOTIVO_PROXIMO_CONTACTO = i.MOTIVO_PROXIMO_CONTACTO;
                cont.CONOCIMIENTO_ID = i.CONOCIMIENTO_ID;
                cont.FECHA_PROXIMO_CONTACTO = Convert.ToDateTime(i.FECHA_PROXIMO_CONTACTO).ToShortTimeString();
                cont.HORA_PROXIMO_CONTACTO = i.HORA_PROXIMO_CONTACTO;
                cont.FECHA_CONTACTO = i.FECHA_CONTACTO;
                cont.NOMBRE_CONTACTO = i.NOMBRE_CONTACTO;
                cont.TIPO_CONTACTO = i.TIPO_CONTACTO;
                contactos.Add(cont);
            }
            return View(contactos);
        }




        //[OutputCache(Duration = 300, VaryByParam = "none")] //cached for 300 seconds 
        public ActionResult Crear(int id)
        {

            var tipo = new SelectList(new[]
                                          {
                                              new {ID="",Name="--SELECCIONE EL TIPO DE CONTACTO"},
                                              new {ID="LLAMADA",Name="LLAMADA"},
                                              new{ID="VISITA",Name="VISITA"},
                                              new{ID="PRESENCIAL",Name="PRESENCIAL"},
                                              new{ID="WHATSAPP",Name="WHATSAPP"},
                                              new{ID="CORREO ELECTRÓNICO",Name="CORREO ELECTRÓNICO"},
                                          },
               "ID", "Name", 1);
            ViewData["tipos"] = tipo;

            var cliente = db.CONOCIMIENTO.Find(id);
            List<PersonaContacto> persona = new List<PersonaContacto>();
            if (cliente.CONTACTO_PRINCIPAL != null && cliente.CONTACTO_PRINCIPAL != "//")
            {
                PersonaContacto p1 = new PersonaContacto();
                string[] pc = cliente.CONTACTO_PRINCIPAL.Split('/');
                p1.Contacto = pc[0];
                persona.Add(p1);
            }
            if (cliente.CONTACTO_ALIADO != null && cliente.CONTACTO_ALIADO!= "//")
            {
                PersonaContacto p2 = new PersonaContacto();
                string[] pc = cliente.CONTACTO_ALIADO.Split('/');
                p2.Contacto = pc[0];
                persona.Add(p2);
            }

            if (cliente.CONTACTO_TECNICO != null && cliente.CONTACTO_TECNICO != "//")
            {
                PersonaContacto p3 = new PersonaContacto();
                string[] pc = cliente.CONTACTO_TECNICO.Split('/');
                p3.Contacto = pc[0];
                persona.Add(p3);
            }


            persona.Add(new PersonaContacto { Contacto = "Otro" });

            ViewBag.LC = persona.Count();
            var contacts = new SelectList(persona, "Contacto", "Contacto");
            ViewData["contactos"] = contacts;


            ViewBag.Cliente = id;
            ViewBag.Ncli = cliente.RAZON_SOCIAL;
            var query_comp = (from c in db.CONTACTOes
                         join cp in db.COMPROMISOes
                         on c.ID equals cp.CONTACTO_ID
                         where c.CONOCIMIENTO_ID == id && cp.FECHA_COMPROMISO >= DateTime.Now
                         select new { cp.FECHA_COMPROMISO }).ToList();

            var query_prox = (from c in db.CONTACTOes
                              join cp in db.PROXIMO_CONTACTO
                              on c.ID equals cp.CONTACTO_ID
                              where c.CONOCIMIENTO_ID == id && cp.FECHA_PROXIMO_CONTACTO >= DateTime.Now
                              select new { cp.FECHA_PROXIMO_CONTACTO }).ToList();

            if (query_comp.Any())
            {
                ViewBag.Vigente = "S";
            }else if (query_prox.Any())
            {
                ViewBag.Vigente = "N";
            }
            return View();
        }

        [HttpPost]
        public ActionResult Crear(ContactoModelView vista)
        {
            var tipo = new SelectList(new[]
                                           {
                                              new {ID="",Name="--SELECCIONE EL TIPO DE CONTACTO"},
                                              new {ID="LLAMADA",Name="LLAMADA"},
                                              new{ID="VISITA",Name="VISITA"},
                                              new{ID="PRESENCIAL",Name="PRESENCIAL"},
                                              new{ID="WHATSAPP",Name="WHATSAPP"},
                                              new{ID="CORREO ELECTRÓNICO",Name="CORREO ELECTRÓNICO"},
                                          },
                "ID", "Name", 1);
            ViewData["tipos"] = tipo;
            if (ModelState.IsValid)
            {
                using(var transaccion = db.Database.BeginTransaction())
                {
                    try
                    {
                        CONTACTO contacto = new CONTACTO();
                        contacto.NOMBRE_CONTACTO = vista.NOMBRE_CONTACTO=="Otro" ? vista.OTRO_CONTACTO : vista.NOMBRE_CONTACTO;
                        contacto.TIPO_CONTACTO = vista.TIPO_CONTACTO;
                        contacto.COMENTARIO = vista.COMENTARIO;
                        contacto.CONOCIMIENTO_ID = vista.CONOCIMIENTO_ID;
                        contacto.FECHA_CONTACTO = DateTime.Now;


                        if (vista.AgregarCompromiso == "s")
                        {
                            contacto.COMPROMISOes.Add(new COMPROMISO
                            {
                                COMPROMISO1 = vista.COMPROMISO,
                                FECHA_COMPROMISO = vista.FECHA_COMPROMISO
                            });
                        }else if(vista.AgregarCompromiso == "n")
                        {
                            contacto.PROXIMO_CONTACTO.Add(new PROXIMO_CONTACTO
                            {
                                FECHA_PROXIMO_CONTACTO = Convert.ToDateTime(vista.FECHA_PROXIMO_CONTACTO),
                                HORA_PROXIMO_CONTACTO = vista.HORA_PROXIMO_CONTACTO,
                                MOTIVO_PROXIMO_CONTACTO = vista.MOTIVO_PROXIMO_CONTACTO
                            });
                        }

                        db.Entry(contacto).State = EntityState.Added;
                        db.SaveChanges();
                        transaccion.Commit();
                        return RedirectToAction("Detail/" + vista.CONOCIMIENTO_ID);

                    }
                    catch (Exception ex)
                    {
                        transaccion.Rollback();
                        ViewBag.Error = ex.InnerException;
                        return View();
                        //throw;
                    }
                }
            }

            ViewBag.Valorc = vista.AgregarCompromiso;
            return View(vista);
            
        }



        public ActionResult Contacto(int id)
        {
            var query = (from c in db.CONTACTOes 
                         join cn in db.CONOCIMIENTO
                         on c.CONOCIMIENTO_ID equals cn.ID
                         join cp in db.COMPROMISOes
                         on c.ID equals cp.CONTACTO_ID into tmpSols
                         join pc in db.PROXIMO_CONTACTO
                         on c.ID equals pc.CONTACTO_ID into tmpProx
                         where c.ID == id
                         from cp in tmpSols.DefaultIfEmpty()
                         from pc in tmpProx.DefaultIfEmpty()
                         select new
                         {
                             cn.RAZON_SOCIAL,
                             c.ID,
                             c.NOMBRE_CONTACTO,
                             c.TIPO_CONTACTO,
                             c.FECHA_CONTACTO,
                             c.COMENTARIO,
                             cp.COMPROMISO1,
                             cp.FECHA_COMPROMISO,
                             pc.MOTIVO_PROXIMO_CONTACTO,
                             pc.FECHA_PROXIMO_CONTACTO,
                             pc.HORA_PROXIMO_CONTACTO
                         }).FirstOrDefault();

            ViewBag.Ncli = query.RAZON_SOCIAL;
            ContactoModelView contacto = new ContactoModelView();
            contacto.CONOCIMIENTO_ID = query.ID;
            contacto.NOMBRE_CONTACTO = query.NOMBRE_CONTACTO;
            contacto.TIPO_CONTACTO = query.TIPO_CONTACTO;
            contacto.FECHA_CONTACTO = query.FECHA_CONTACTO;
            contacto.COMENTARIO = query.COMENTARIO;
            contacto.COMPROMISO = query.COMPROMISO1;
            contacto.FECHA_COMPROMISO = query.FECHA_COMPROMISO;
            contacto.MOTIVO_PROXIMO_CONTACTO = query.MOTIVO_PROXIMO_CONTACTO;
            contacto.FECHA_PROXIMO_CONTACTO = Convert.ToDateTime(query.FECHA_PROXIMO_CONTACTO).ToShortTimeString();
            contacto.HORA_PROXIMO_CONTACTO = query.HORA_PROXIMO_CONTACTO;

            return View(contacto);
        }










        public ActionResult EditCompromiso(int id,string estado)
        {
            
            ViewBag.Estatus = estado.ToUpper();
            var tipo = new SelectList(new[]
                                          {
                                              new {ID="",Name="--SELECCIONE EL TIPO DE CONTACTO"},
                                              new {ID="LLAMADA",Name="LLAMADA"},
                                              new{ID="VISITA",Name="VISITA"},
                                              new{ID="PRESENCIAL",Name="PRESENCIAL"},
                                              new{ID="WHATSAPP",Name="WHATSAPP"},
                                              new{ID="CORREO ELECTRÓNICO",Name="CORREO ELECTRÓNICO"},
                                          },
               "ID", "Name", 1);
            ViewData["tipos"] = tipo;
            var compromiso = db.COMPROMISOes.Find(id);
            var razon = db.CONOCIMIENTO.Find(compromiso.CONTACTO.CONOCIMIENTO_ID).RAZON_SOCIAL;
            ViewBag.Razon = razon;
            return View(compromiso);
        }

        [HttpPost]
        public ActionResult EditCompromiso(COMPROMISO comp)
        {
            var tipo = new SelectList(new[]
                                          {
                                              new {ID="",Name="--SELECCIONE EL TIPO DE CONTACTO"},
                                              new {ID="LLAMADA",Name="LLAMADA"},
                                              new{ID="VISITA",Name="VISITA"},
                                              new{ID="PRESENCIAL",Name="PRESENCIAL"},
                                              new{ID="WHATSAPP",Name="WHATSAPP"},
                                              new{ID="CORREO ELECTRÓNICO",Name="CORREO ELECTRÓNICO"},
                                          },
               "ID", "Name", 1);
            ViewData["tipos"] = tipo;

            if (ModelState.IsValid)
            {
                using(var tra = db.Database.BeginTransaction())
                {
                    try
                    {
                        var query = db.COMPROMISOes.Find(comp.ID);
                        query.COMPROMISO1 = comp.COMPROMISO1;
                        query.FECHA_COMPROMISO = comp.FECHA_COMPROMISO;
                        //query.CONTACTO.NOMBRE_CONTACTO = comp.CONTACTO.NOMBRE_CONTACTO;
                        db.SaveChanges();


                        var contacto = db.CONTACTOes.Find(comp.CONTACTO_ID);
                        contacto.COMENTARIO = comp.CONTACTO.COMENTARIO;
                        contacto.TIPO_CONTACTO = comp.CONTACTO.TIPO_CONTACTO;
                        db.SaveChanges();

                        tra.Commit();
                        return RedirectToAction("Inicio");
                    }
                    catch (Exception ex)
                    {
                        tra.Rollback();
                        ViewBag.Error = ex.InnerException;
                        return View(comp);
                    }
                }
            }
            return View(comp);
            
        }






        public ActionResult EditProximo(int id,string estado)
        {
            
            ViewBag.Estatus = estado.ToUpper();
            var tipo = new SelectList(new[]
                                          {
                                              new {ID="",Name="--SELECCIONE EL TIPO DE CONTACTO"},
                                              new {ID="LLAMADA",Name="LLAMADA"},
                                              new{ID="VISITA",Name="VISITA"},
                                              new{ID="PRESENCIAL",Name="PRESENCIAL"},
                                              new{ID="WHATSAPP",Name="WHATSAPP"},
                                              new{ID="CORREO ELECTRÓNICO",Name="CORREO ELECTRÓNICO"},
                                          },
               "ID", "Name", 1);
            ViewData["tipos"] = tipo;
            var proximo = db.PROXIMO_CONTACTO.Find(id);
            var razon = db.CONOCIMIENTO.Find(proximo.CONTACTO.CONOCIMIENTO_ID).RAZON_SOCIAL;
            ViewBag.Razon = razon;
            return View(proximo);
        }

        [HttpPost]
        public ActionResult EditProximo(PROXIMO_CONTACTO prox)
        {
            if (ModelState.IsValid)
            {
                using(var tra = db.Database.BeginTransaction())
                {
                    try
                    {
                        var proximo = db.PROXIMO_CONTACTO.Find(prox.ID);
                        proximo.FECHA_PROXIMO_CONTACTO = prox.FECHA_PROXIMO_CONTACTO;
                        proximo.HORA_PROXIMO_CONTACTO = prox.HORA_PROXIMO_CONTACTO;
                        proximo.MOTIVO_PROXIMO_CONTACTO = prox.MOTIVO_PROXIMO_CONTACTO;
                        db.SaveChanges();


                        var con = db.CONTACTOes.Find(prox.CONTACTO_ID);
                        con.COMENTARIO = prox.CONTACTO.COMENTARIO;
                        con.TIPO_CONTACTO = prox.CONTACTO.TIPO_CONTACTO;
                        db.SaveChanges();

                        tra.Commit();

                        if (Convert.ToInt32(Session["perfil"]) != 1)
                        {
                            return RedirectToAction("Inicio/" + Session["usuario"].ToString());
                        }else
                        {
                            return RedirectToAction("Inicio");
                        }
                        
                    }
                    catch (Exception ex)
                    {
                        tra.Rollback();
                        ViewBag.Error = ex.InnerException;
                        return RedirectToAction("Inicio");

                    }
                    
                }
            }
            return View(prox);

        }
    }
}