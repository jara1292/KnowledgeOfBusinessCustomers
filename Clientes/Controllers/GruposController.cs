﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Clientes.Models;

namespace Clientes.Controllers
{
    public class GruposController : Controller
    {
        private EmpresarialesModel db = new EmpresarialesModel();

        // GET: Grupos
        public async Task<ActionResult> Index()
        {
            return View(await db.GRUPOS.ToListAsync());
        }

        // GET: Grupos/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GRUPO gRUPO = await db.GRUPOS.FindAsync(id);
            if (gRUPO == null)
            {
                return HttpNotFound();
            }
            return View(gRUPO);
        }

        // GET: Grupos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Grupos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(GRUPO gRUPO)
        {
            if (ModelState.IsValid)
            {
                using(var transaccion = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.GRUPOS.Add(gRUPO);
                        await db.SaveChangesAsync();
                        transaccion.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        transaccion.Rollback();
                        ViewBag.Error = "Error: " + ex.InnerException;
                        return View(gRUPO);
                    }
                }
                
            }

            return View(gRUPO);
        }

        // GET: Grupos/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GRUPO gRUPO = await db.GRUPOS.FindAsync(id);
            if (gRUPO == null)
            {
                return HttpNotFound();
            }
            return View(gRUPO);
        }

        // POST: Grupos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,RAZON")] GRUPO gRUPO)
        {
            if (ModelState.IsValid)
            {
                db.Entry(gRUPO).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(gRUPO);
        }

        // GET: Grupos/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GRUPO gRUPO = await db.GRUPOS.FindAsync(id);
            if (gRUPO == null)
            {
                return HttpNotFound();
            }
            return View(gRUPO);
        }

        // POST: Grupos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            GRUPO gRUPO = await db.GRUPOS.FindAsync(id);
            db.GRUPOS.Remove(gRUPO);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
