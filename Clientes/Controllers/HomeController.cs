﻿using Clientes.Models;
using System.Linq;
using System.Web.Mvc;

namespace Clientes.Controllers
{
    public class HomeController : Controller
    {
        private EmpresarialesModel db = new EmpresarialesModel();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }



        //public JsonResult GetTiposCliente()
        //{
        //    var query = from s in db.CONOCIMIENTO select s.TIPO;
        //    return Json(query.ToList().Distinct(), JsonRequestBehavior.AllowGet);
        //}
    }
}