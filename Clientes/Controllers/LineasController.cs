﻿using Clientes.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;

namespace Clientes.Controllers
{
    public class LineasController : Controller
    {
        private EmpresarialesModel db = new EmpresarialesModel();
        // GET: Lineas
        public ActionResult Index()
        {
            var id = Convert.ToInt16(Request.QueryString["id"]);
            var razon = db.CONOCIMIENTO.Find(id);
            var query = db.Database.SqlQuery<LineasViewModel>("SELECT CICLO,CLAVE_PLAN,CUENTA,DURACION,EJECUTIVO,ESTATUS_ADENDUM,ESTATUS_CUENTA,FECHA_INICIO,FECHA_TERMINO,ICCID,IMEI,MESES_RESTANTES,MODELO,MONTO_RENTA,NOMBRE_PLAN,PRODUCTO_CC,RAZON_SOCIAL,REGION,RFC,TELEFONO,TIPO,ESTATUS_LINEA FROM EMPRESARIALES.ADENDA WHERE RFC=@razon", new SqlParameter("@razon", razon.RFC)).ToList();
            List<LineasViewModel> regs = new List<LineasViewModel>();

            ViewBag.RazonScoial = query.First().RAZON_SOCIAL;
            foreach(var item in query)
            {
                LineasViewModel reg = new LineasViewModel();
                reg.CICLO = item.CICLO;
                reg.CLAVE_PLAN = item.CLAVE_PLAN;
                reg.CUENTA = item.CLAVE_PLAN;
                reg.CUENTA_PADRE = item.CUENTA_PADRE;
                reg.DURACION = item.DURACION;
                reg.EJECUTIVO = item.EJECUTIVO;
                reg.EQUIPO = item.EQUIPO;
                reg.ESTATUS_ADENDUM = item.ESTATUS_ADENDUM;
                reg.ESTATUS_CUENTA = item.ESTATUS_CUENTA;
                reg.FECHA_INICIO = item.FECHA_INICIO;
                reg.FECHA_TERMINO = item.FECHA_TERMINO;
                reg.ICCID = item.ICCID;
                reg.IMEI = item.IMEI;
                reg.MESES_RESTANTES = item.MESES_RESTANTES;
                reg.MODELO = item.MODELO;
                reg.MONTO_RENTA = item.MONTO_RENTA;
                reg.NOMBRE_PLAN = item.NOMBRE_PLAN;
                reg.PRODUCTO_CC = item.PRODUCTO_CC;
                reg.RAZON_SOCIAL = item.RAZON_SOCIAL;
                reg.REGION = item.REGION;
                reg.RFC = item.RFC;
                reg.TELEFONO = item.TELEFONO;
                reg.TIPO = item.TIPO;
                reg.ESTATUS_LINEA = item.ESTATUS_LINEA;

                regs.Add(reg);
            }


            return View(regs);
            //return PartialView("_Lineas",regs);
        }
    }
}