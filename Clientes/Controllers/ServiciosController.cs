﻿using Clientes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Clientes.Controllers
{
    public class ServiciosController : Controller
    {
        // GET: Servicios
        public ActionResult Index()
        {
            return View();
        }


        public JsonResult ComprobarRFC(string rfc)
        {
            using (var db = new EmpresarialesModel())
            {
                var query = db.CONOCIMIENTO.Where(x => x.RFC == rfc);

                if (query.Any())
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }else
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}