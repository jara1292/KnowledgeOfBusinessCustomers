﻿using System.Data.Entity;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using Clientes.Models;
using System;

namespace Clientes.Controllers
{
    public class SituacionController : Controller
    {
        private EmpresarialesModel db = new EmpresarialesModel();

        // GET: Situacion
        public async Task<ActionResult> Index()
        {
            return View(await db.SITUACION.ToListAsync());
        }

        // GET: Situacion/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SITUACION sITUACION = await db.SITUACION.FindAsync(id);
            if (sITUACION == null)
            {
                return HttpNotFound();
            }
            return View(sITUACION);
        }

        // GET: Situacion/Create
        public ActionResult Create()
        {
            ViewBag.id = Request.QueryString["id"];
            return View();
        }

        // POST: Situacion/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(SITUACION sITUACION)
        {
            var url = Request.Url.ToString();
            if (ModelState.IsValid)
            {
                db.SITUACION.Add(sITUACION);
                await db.SaveChangesAsync();
                //return RedirectToAction("Index");
                //return Redirect();
                if (Convert.ToInt32(Session["perfil"]) == 1)
                {
                    //var url = Request.Url.ToString();
                    if (url.Contains("201.144.210.148"))
                    {
                        return Redirect("/activaciones/Empresariales/Clientes/Cliente/Inicio");
                    }
                    return Redirect("/Empresariales/Clientes/Cliente/Inicio");
                }else if (Convert.ToInt32(Session["perfil"]) == 2)
                {
                    if (url.Contains("201.144.210.148"))
                    {
                        return Redirect("/activaciones/Empresariales/Clientes/Cliente/EJECUTIVO?usuario=" + Session["idusuario"]);
                    }
                    return Redirect("/Empresariales/Clientes/Cliente/EJECUTIVO?usuario=" + Session["idusuario"]);
                }else
                {
                    if (url.Contains("201.144.210.148"))
                    {
                        return Redirect("/activaciones/Empresariales/Clientes/Cliente/Login");
                    }
                    return Redirect("/Empresariales/Clientes/Cliente/Login");
                }
                
            }

            return View(sITUACION);
        }

        // GET: Situacion/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //var query = from s in db.SITUACION where s.IDCONOCIMIENTO == id select s;

            //SITUACION sITUACION = await db.SITUACION.FindAsync(query.FirstOrDefault().IDCONOCIMIENTO);
            SITUACION sITUACION = await db.SITUACION.FindAsync(id);
            if (sITUACION == null)
            {
                //return HttpNotFound();
                ViewBag.id = id;
                return View("Create");
            }
            ViewBag.id = Request.QueryString["idc"];
            return View(sITUACION);
        }




        // GET: Situacion/Edit/5
        public async Task<ActionResult> Editar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            SITUACION sITUACION = await db.SITUACION.FindAsync(id);
            if (sITUACION == null)
            {
                ViewBag.id = id;
                return View("Create");
            }
            ViewBag.id = Request.QueryString["idc"];
            return PartialView("_Editar", sITUACION);
        }

        // POST: Situacion/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(SITUACION sITUACION)
        {
            var url = Request.Url.ToString();
            

            if (ModelState.IsValid)
            {
                db.Entry(sITUACION).State = EntityState.Modified;
                await db.SaveChangesAsync();
                //return RedirectToAction("Index");
                if (Convert.ToInt32(Session["perfil"]) == 1)
                {
                    if (url.Contains("201.144.210.148"))
                    {
                        return Redirect("/activaciones/Empresariales/Clientes/Cliente/Inicio");
                    }
                    return Redirect("/Empresariales/Clientes/Cliente/Inicio");
                }
                else if (Convert.ToInt32(Session["perfil"]) == 2)
                {
                    if (url.Contains("201.144.210.148"))
                    {
                        return Redirect("/activaciones/Empresariales/Clientes/Cliente/EJECUTIVO?usuario=" + Session["idusuario"]);
                    }
                    return Redirect("/Empresariales/Clientes/Cliente/EJECUTIVO?usuario=" + Session["idusuario"]);
                }
                else
                {
                    if (url.Contains("201.144.210.148"))
                    {
                        return Redirect("/activaciones/Empresariales/Clientes/Cliente/Login");
                    }
                    return Redirect("/Empresariales/Clientes/Cliente/Login");
                }
            }
            return View(sITUACION);
        }

        // GET: Situacion/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SITUACION sITUACION = await db.SITUACION.FindAsync(id);
            if (sITUACION == null)
            {
                return HttpNotFound();
            }
            return View(sITUACION);
        }

        // POST: Situacion/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            SITUACION sITUACION = await db.SITUACION.FindAsync(id);
            db.SITUACION.Remove(sITUACION);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
