﻿using Clientes.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Clientes.Logica
{
    public class ClienteLogic
    {
        public bool Registrar(CONOCIMIENTO cliente)
        {
            try
            {
                using (var context = new EmpresarialesModel())
                {
                    context.Entry(cliente).State = EntityState.Added;
                    context.SaveChanges();
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }


        public bool Actualizar(CONOCIMIENTO cliente)
        {
            try
            {
                using(var db = new EmpresarialesModel())
                {
                    db.Entry(cliente).State = EntityState.Modified;
                    db.SaveChanges();

                }

            }
            catch (Exception)
            {

                //throw;
                return false;
            }
            return true;
        }


        public CONOCIMIENTO Obtener(int id)
        {
            using (var context = new EmpresarialesModel())
            {
                // Esta consulta incluye el detalle de conocimiento, y el conocimiento_pv que tiene cada registro de conocimiento. Me refiero a sus relaciones
                return context.CONOCIMIENTO.Include(x => x.CONOCIMIENTO_PV.Select(y => y.DIRECCION))
                                          .Where(x => x.ID == id)
                                          .SingleOrDefault();

            }
        }

        public List<CONOCIMIENTO> Listar()
        {
            using (var context = new EmpresarialesModel())
            {
                return context.CONOCIMIENTO.OrderByDescending(x => x.EJECUTIVO_FONIX)
                                          .ToList();
            }
        }
    }
}