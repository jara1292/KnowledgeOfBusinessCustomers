﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Clientes.Models
{
    [Table("EMPRESARIALES.ADENDA")]
    public class ADENDA
    {
        public string TIPO { get; set; }
        public string REGION { get; set; }
        public string CUENTA_PADRE { get; set; }
        public string CUENTA { get; set; }
        public int? CICLO { get; set; }
        public string RFC { get; set; }
        public string RAZON_SOCIAL { get; set; }
        public string TELEFONO { get; set; }
        public string ICCID { get; set; }
        public string IMEI { get; set; }
        public string ESTATUS_CUENTA { get; set; }
        public string PRODUCTO_CC { get; set; }
        public string CLAVE_PLAN { get; set; }
        public string NOMBRE_PLAN { get; set; }
        public decimal? MONTO_RENTA { get; set; }
        public string EQUIPO { get; set; }
        public string MODELO { get; set; }
        public int? DURACION { get; set; }
        public DateTime? FECHA_INICIO { get; set; }
        public DateTime? FECHA_TERMINO { get; set; }
        public string ESTATUS_ADENDUM { get; set; }
        public int? MESES_RESTANTES { get; set; }
        public string EJECUTIVO { get; set; }



        private double det()
        {
            var meses = Math.Abs((DateTime.Now.Month - Convert.ToDateTime(FECHA_TERMINO).Month) + 12 * (DateTime.Now.Year - Convert.ToDateTime(FECHA_TERMINO).Year));
            var t = FECHA_TERMINO == null ? 0 : FECHA_TERMINO < DateTime.Now ? 0 : meses < 0 ? 0 : meses;
            //if (FECHA_TERMINO == null)
            //{
            //    return 0;
            //}
            //else if (FECHA_TERMINO < DateTime.Now)
            //{
            //    return 0;
            //}
            //else if (meses < 0)
            //{
            //    return 0;
            //}
            //else
            //{
            //    return meses;
            //}
            return t;
        }


        [NotMapped]
        //public int MESES { get { return Math.Abs((DateTime.Now.Month - Convert.ToDateTime(FECHA_TERMINO).Month) + 12 * (DateTime.Now.Year - Convert.ToDateTime(FECHA_TERMINO).Year)); ; } }
        public double MESES { get { return det(); } }

        [Key]
        public int Id { get; set; }

        public string GRUPO { get; set; }
        public DateTime? CREATEDATE { get; set; }
        public string ESTATUS_LINEA { get; set; }

        [NotMapped]
        public decimal? monto { get { return MONTO_RENTA + (MONTO_RENTA * Convert.ToDecimal(0.16)); } }
    }
}