namespace Clientes.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EMPRESARIALES.COMPROMISO")]
    public partial class COMPROMISO
    {
        [Key]
        public int ID { get; set; }

        public int? CONTACTO_ID { get; set; }

        [Column("COMPROMISO", TypeName = "text")]
        //Crear textarea
        [DataType(DataType.MultilineText)]
        [Display(Name = "Compromiso")]
        [Required]
        public string COMPROMISO1 { get; set; }

        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Fecha compromiso")]
        [Required]
        public DateTime? FECHA_COMPROMISO { get; set; }

        public virtual CONTACTO CONTACTO { get; set; }
    }
}
