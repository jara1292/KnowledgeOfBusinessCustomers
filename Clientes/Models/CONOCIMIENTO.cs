namespace Clientes.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.SqlClient;
    using System.Linq;


    [Table("EMPRESARIALES.CONOCIMIENTO")]
    public partial class CONOCIMIENTO
    {
        private EmpresarialesModel dbs = new EmpresarialesModel();
        public CONOCIMIENTO()
        {
            //CONOCIMIENTO_PV = new HashSet<CONOCIMIENTO_PV>();
            CONOCIMIENTO_PV = new List<CONOCIMIENTO_PV>();
        }

        public DateTime? FECHA_ACTUALIZA { get; set; }

        //[Required(ErrorMessage = "El campo {0} es obligatorio")]
        //[StringLength(30)]
        //public string TIPO { get; set; }

        [StringLength(175)]
        public string RAZON_SOCIAL { get; set; }

        [StringLength(150)]
        public string NOMBRE_COMERCIAL { get; set; }

        [StringLength(13)]
        public string RFC { get; set; }

        public int? ANIOS_MERCADO { get; set; }

        [Display(Name = "Giro de la empresa")]
        public string GIRO { get; set; }


        [StringLength(60)]
        [Display(Name = "Subtipo Giro)")]
        public string SUBTIPO_GIRO { get; set; }

        [StringLength(300)]
        [Display(Name = "Actividad principal de la empresa")]
        public string DESC_ACTIVIDAD { get; set; }

        [StringLength(300)]
        public string COMPETENCIA { get; set; }

        [StringLength(300)]
        public string PRINCIPALES_CLIENTES { get; set; }

        [StringLength(30)]
        public string PAGINA { get; set; }

        [StringLength(10)]
        public string TEL_EMPRESA { get; set; }

        //[StringLength(175)]
        //public string DIRECCION { get; set; }

        public int? NO_EMPLEADOS { get; set; }

        [StringLength(150)]
        public string REP_LEGAL { get; set; }

        //[StringLength(30)]
        //public string IFE { get; set; }

        [StringLength(250)]
        public string CONTACTO_ALIADO { get; set; }

        [StringLength(50)]
        public string CARGO_ALIADO { get; set; }

        [StringLength(250)]
        public string CONTACTO_TECNICO { get; set; }

        [StringLength(50)]
        public string CARGO_TECNICO { get; set; }

        [StringLength(150)]
        public string CONTACTO_PRINCIPAL { get; set; }

        [StringLength(50)]
        public string CARGO_PRINCIPAL { get; set; }

        [StringLength(250)]
        public string CONTACTO_CXP { get; set; }

        [StringLength(50)]
        public string CARGO_CXP { get; set; }

        [StringLength(100)]
        [Display(Name = "EJECUTIVO F�NIX")]
        public string EJECUTIVO_FONIX { get; set; }



        [Display(Name = "Clave NISI")]
        public string CLAVE_NISI { get; set; }

        [Display(Name = "Clave Portal")]
        public string CLAVE_PORTAL { get; set; }

        [Display(Name = "Usuario Portal")]
        public string USUARIO_PORTAL { get; set; }

        [Display(Name = "Alias Portal")]
        public string ALIAS_PORTAL { get; set; }

        [Display(Name = "Calve Interbancaria")]
        public string CLAVE_INTERBANCARIA { get; set; }

        [Display(Name = "Referencia RAP")]
        public string REFERENCIA_RAP { get; set; }

        [Display(Name = "Esporadico")]
        public string ESPORADICO { get; set; }

        [Display(Name = "Mercado")]
        public string MERCADO { get; set; }

        [Display(Name = "Folio verificaci�n")]
        public string FOLIO_VERIFICACION { get; set; }

        [Display(Name = "Proveedores")]
        public string PROVEEDORES { get; set; }

        public DateTime? CREATEDATE { get { return DateTime.Now; } }

        public int ID { get; set; }

        [Display(Name = "Grupo")]
        public int? GRUPO_ID { get; set; }



        private string det()
        {
            if (!String.IsNullOrEmpty(FOLIO_VERIFICACION))
            {
                return "Nuevo";
            }
            else if (!String.IsNullOrEmpty(ESPORADICO))
            {
                return "Cliente";
            }
            else
            {
                var activacion = dbs.Database.SqlQuery<Sols>("SELECT Solicitud FROM Planes.dbo.Solicitud_Activaciones WHERE RFC=@rfc AND EstatusSol='A'", new SqlParameter("@rfc", RFC));

                if (activacion.Any())
                {
                    return "Cliente";
                }else
                {
                    return "Prospecto";
                }
            }
        }


        private string vend()
        {
            if (!string.IsNullOrEmpty(EJECUTIVO_FONIX))
            {
                var query2 = dbs.COUSUARIOS.Where(x => x.VENDEDOR == EJECUTIVO_FONIX).FirstOrDefault();
                if (!string.IsNullOrEmpty(query2.NOMBRE))
                {
                    return query2.NOMBRE;
                }
                else
                {
                    return "---";
                }
            }
            else
            {
                return "---";
            }

        }



        private string Group()
        {
            //var query = dbs.ADENDAs.Where(x => x.RFC == RFC).FirstOrDefault();
            var query = dbs.GRUPOS.Where(x => x.ID == GRUPO_ID).FirstOrDefault();

            if (query != null)
            {
                return query.RAZON;
            }

            return "---";
        }

        [NotMapped]
        [Display(Name = "Detalles")]
        public string Detail { get { return det(); } }

        [NotMapped]
        [Display(Name = "Ejecutivo")]
        public string Vendedor { get { return vend(); } }

        [NotMapped]
        [Display(Name = "Grupo")]
        public string Grupo { get { return Group(); } }


        //public virtual SITUACION SITUACION { get; set; }

        //public virtual ICollection<CONOCIMIENTO_PV> CONOCIMIENTO_PV { get; set; }
        public virtual List<CONOCIMIENTO_PV> CONOCIMIENTO_PV { get; set; }


        #region ViewModels
        public class ConocimientoViewModel
        {
            public int ID { get; set; }

            //[Required(ErrorMessage = "El campo {0} es obligatorio")]
            [Display(Name = "Fecha de actualizaci�n")]
            //[DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
//            [DataType(DataType.Date)]
            public DateTime? FECHA_ACTUALIZA { get {return DateTime.Now; } }

            //[Required(ErrorMessage = "El campo {0} es obligatorio")]
            //[Display(Name = "Tipo de Cliente")]
            //[StringLength(30)]
            //public string TIPO { get; set; }

            [StringLength(175)]
            [Required(ErrorMessage = "El campo {0} es obligatorio")]
            [Display(Name = "Razon Social")]
            public string RAZON_SOCIAL { get; set; }

            [StringLength(150)]
            [Required(ErrorMessage = "El campo {0} es obligatorio")]
            [Display(Name = "Nombre Comercial")]
            public string NOMBRE_COMERCIAL { get; set; }

            [StringLength(13, ErrorMessage = "El campo {0} debe tener entre {2} y {1} caracteres", MinimumLength = 12)]
            [Required(ErrorMessage = "El campo {0} es obligatorio")]
            [Display(Name = "RFC")]
            public string RFC { get; set; }

            //[Required(ErrorMessage = "El campo {0} es obligatorio")]
            [Display(Name = "Tiempo presencia en el mercado(a�os)")]
            public int? ANIOS_MERCADO { get; set; }

            //[Required(ErrorMessage = "El campo {0} es obligatorio")]
            [Display(Name = "Giro de la empresa")]
            public string GIRO { get; set; }


            [StringLength(60)]
            [Display(Name = "Subtipo (Giro)")]
            public string SUBTIPO_GIRO { get; set; }

            [StringLength(300)]
            //[Required(ErrorMessage = "El campo {0} es obligatorio")]
            [Display(Name = "Actividad de la empresa")]
            public string DESC_ACTIVIDAD { get; set; }

            [StringLength(300)]
            //[Required(ErrorMessage = "El campo {0} es obligatorio")]
            [Display(Name = "Competencia")]
            public string COMPETENCIA { get; set; }

            [StringLength(300)]
            //[Required(ErrorMessage = "El campo {0} es obligatorio")]
            [Display(Name = "Principales Clientes")]
            public string PRINCIPALES_CLIENTES { get; set; }

            [StringLength(30)]
            //[Required(ErrorMessage = "El campo {0} es obligatorio")]
            //[DataType(DataType.Url)]
            [Display(Name = "P�gina web")]
            public string PAGINA { get; set; }

            [StringLength(10, ErrorMessage = "El campo {0} debe tener {1} caracteres", MinimumLength = 10)]
            //[Required(ErrorMessage = "El campo {0} es obligatorio")]
            [Display(Name = "T�l. empresa")]
            public string TEL_EMPRESA { get; set; }

            //[StringLength(175)]
            ////[Required(ErrorMessage = "El campo {0} es obligatorio")]
            //[Display(Name = "Direcci�n")]
            //public string DIRECCION { get; set; }


            //[Required(ErrorMessage = "El campo {0} es obligatorio")]
            [Display(Name = "N�mero de empleados")]
            public int? NO_EMPLEADOS { get; set; }

            [StringLength(150)]
            //[Required(ErrorMessage = "El campo {0} es obligatorio")]
            [Display(Name = "Representante legal")]
            public string REP_LEGAL { get; set; }

            //[StringLength(30)]
            //[Required(ErrorMessage = "El campo {0} es obligatorio")]
            //[Display(Name = "IFE")]
            //public string IFE { get; set; }

            [StringLength(250)]
            //[Required(ErrorMessage = "El campo {0} es obligatorio")]
            [Display(Name = "Contacto (aliado)")]
            public string CONTACTO_ALIADO { get; set; }

            public string CONTACTO_ALIADO_EXT { get; set; }
            public string CONTACTO_ALIADO_CORREO { get; set; }

            [StringLength(50)]
            //[Required(ErrorMessage = "El campo {0} es obligatorio")]
            [Display(Name = "Cargo")]
            public string CARGO_ALIADO { get; set; }

            [StringLength(250)]
            //[Required(ErrorMessage = "El campo {0} es obligatorio")]
            [Display(Name = "Contacto (t�cnico)")]
            public string CONTACTO_TECNICO { get; set; }

            public string CONTACTO_TECNICO_EXT { get; set; }
            public string CONTACTO_TECNICO_CORREO { get; set; }

            [StringLength(50)]
            //[Required(ErrorMessage = "El campo {0} es obligatorio")]
            [Display(Name = "Cargo")]
            public string CARGO_TECNICO { get; set; }

            [StringLength(150)]
            //[Required(ErrorMessage = "El campo {0} es obligatorio")]
            [Display(Name = "Contacto (principal)")]
            public string CONTACTO_PRINCIPAL { get; set; }

            public string CONTACTO_PRINCIPAL_EXT { get; set; }
            public string CONTACTO_PRINCIPAL_CORREO { get; set; }

            [StringLength(50)]
            //[Required(ErrorMessage = "El campo {0} es obligatorio")]
            [Display(Name = "Cargo")]
            public string CARGO_PRINCIPAL { get; set; }

            [StringLength(250)]
            //[Required(ErrorMessage = "El campo {0} es obligatorio")]
            [Display(Name = "Contacto (cxp)")]
            public string CONTACTO_CXP { get; set; }

            public string CONTACTO_CXP_EXT { get; set; }
            public string CONTACTO_CXP_CORREO { get; set; }

            [StringLength(50)]
            //[Required(ErrorMessage = "El campo {0} es obligatorio")]
            [Display(Name = "Cargo")]
            public string CARGO_CXP { get; set; }

            [StringLength(100)]
            [Required(ErrorMessage = "El campo {0} es obligatorio")]
            [Display(Name = "Ejecutivo F�nix")]
            public string EJECUTIVO_FONIX { get; set; }
            //public DateTime? CREATEDATE { get; set; }


            [Display(Name = "Clave NISI")]
            public string CLAVE_NISI { get; set; }

            [Display(Name = "Clave Portal")]
            public string CLAVE_PORTAL { get; set; }

            [Display(Name = "Usuario Portal")]
            public string USUARIO_PORTAL { get; set; }

            [Display(Name = "Alias Portal")]
            public string ALIAS_PORTAL { get; set; }

            [Display(Name = "Calve Interbancaria")]
            public string CLAVE_INTERBANCARIA { get; set; }

            [Display(Name = "Referencia RAP")]
            public string REFERENCIA_RAP { get; set; }

            [Display(Name = "Esporadico")]
            public string ESPORADICO { get; set; }

            [Display(Name = "Mercado")]
            public string MERCADO { get; set; }

            [Display(Name = "Folio verificaci�n")]
            public string FOLIO_VERIFICACION { get; set; }

            [Display(Name = "Proveedores")]
            public string PROVEEDORES { get; set; }

            [StringLength(13)]
            [Display(Name = "Tipo")]
            public string CabeceraTipo { get; set; }

            [StringLength(175)]
            [Display(Name = "Direcci�n")]
            public string CabeceraDireccion { get; set; }


            ////Direcci�n Fiscal
            [Display(Name = "Calle y No.")]
            public string CALLE_No { get; set; }

            [Display(Name = "Colonia")]
            public string COLONIA { get; set; }

            [Display(Name = "Municipio")]
            public string MUNICIPIO { get; set; }

            [Display(Name = "Estado")]
            public string ESTADO { get; set; }

            [Display(Name = "CP")]
            public string CP { get; set; }


            ////Direcci�n Entrga
            [Display(Name = "Calle y No.")]
            public string CALLE_No2 { get; set; }

            [Display(Name = "Colonia")]
            public string COLONIA2 { get; set; }

            [Display(Name = "Municipio")]
            public string MUNICIPIO2 { get; set; }

            [Display(Name = "Estado")]
            public string ESTADO2 { get; set; }

            [Display(Name = "CP")]
            public string CP2 { get; set; }


            /*Estatus cuenta*/
            [Display(Name = "Estado de cuenta")]
            public string ESTATUS_CUENTA { get; set; }


            [Display(Name = "Grupo")]
            public int? GRUPO_ID { get; set; }

            public List<CONOCIMIENTO_PVViewModel> CONOCIMIENTO_PV { get; set; }

            //public virtual DIRECCION DIRECCION { get; set; }



            public ConocimientoViewModel()
            {
                CONOCIMIENTO_PV = new List<CONOCIMIENTO_PVViewModel>();
                Refrescar();
            }


            public void Refrescar()
            {
                CabeceraTipo = null;
                CabeceraDireccion = null;
            }


            public bool SeAgregoUnPvValido()
            {
                return !(string.IsNullOrEmpty(CabeceraTipo) || string.IsNullOrEmpty(CabeceraDireccion));
            }


            public bool ExisteEnPV(string Id)
            {
                return CONOCIMIENTO_PV.Any(x => x.DIRECCION == Id);
            }

            public void RetirarItemDeDetalle()
            {
                if (CONOCIMIENTO_PV.Count > 0)
                {
                    var detalleARetirar = CONOCIMIENTO_PV.Where(x => x.Retirar)
                                                            .SingleOrDefault();

                    CONOCIMIENTO_PV.Remove(detalleARetirar);
                }
            }




            public void AgregarItemADetalle()
            {
                CONOCIMIENTO_PV.Add(new CONOCIMIENTO_PVViewModel
                {
                    TIPO = CabeceraTipo,
                    DIRECCION = CabeceraDireccion
                });

                //Refrescar();
            }



            public CONOCIMIENTO ToModel()
            {
                var comprobante = new CONOCIMIENTO();
                comprobante.ID = this.ID;
                comprobante.FECHA_ACTUALIZA = this.FECHA_ACTUALIZA;
                comprobante.DESC_ACTIVIDAD = this.DESC_ACTIVIDAD;
                comprobante.GIRO = this.GIRO;
                comprobante.SUBTIPO_GIRO = this.SUBTIPO_GIRO;
                //comprobante.DIRECCION = this.DIRECCION;
                comprobante.EJECUTIVO_FONIX = this.EJECUTIVO_FONIX;
                //comprobante.CREATEDATE = DateTime.Now;
                comprobante.ANIOS_MERCADO = this.ANIOS_MERCADO;
                comprobante.CARGO_ALIADO = this.CARGO_ALIADO;
                comprobante.CARGO_CXP = this.CARGO_CXP;
                comprobante.CARGO_PRINCIPAL = this.CARGO_PRINCIPAL;
                comprobante.CARGO_TECNICO = this.CARGO_PRINCIPAL;
                comprobante.COMPETENCIA = this.COMPETENCIA;
                comprobante.CONTACTO_ALIADO = this.CONTACTO_ALIADO + "/" + this.CONTACTO_ALIADO_EXT + "/" + this.CONTACTO_ALIADO_CORREO;
                comprobante.CONTACTO_CXP = this.CONTACTO_CXP + "/" + this.CONTACTO_CXP_EXT + "/" + this.CONTACTO_CXP_CORREO;
                comprobante.CONTACTO_PRINCIPAL = this.CONTACTO_PRINCIPAL + "/" + this.CONTACTO_PRINCIPAL_EXT + "/" + this.CONTACTO_PRINCIPAL_CORREO;
                comprobante.CONTACTO_TECNICO = this.CONTACTO_TECNICO + "/" + this.CONTACTO_TECNICO_EXT + "/" + this.CONTACTO_TECNICO_CORREO;
                comprobante.NOMBRE_COMERCIAL = this.NOMBRE_COMERCIAL;
                comprobante.NO_EMPLEADOS = this.NO_EMPLEADOS;
                comprobante.PAGINA = this.PAGINA;
                comprobante.PRINCIPALES_CLIENTES = this.PRINCIPALES_CLIENTES;
                comprobante.RAZON_SOCIAL = this.RAZON_SOCIAL;
                comprobante.REP_LEGAL = this.REP_LEGAL;
                comprobante.RFC = this.RFC;
                comprobante.TEL_EMPRESA = this.TEL_EMPRESA;
                //comprobante.TIPO = this.TIPO;
                comprobante.CLAVE_NISI = this.CLAVE_NISI;
                comprobante.CLAVE_PORTAL = this.CLAVE_PORTAL;
                comprobante.USUARIO_PORTAL = this.USUARIO_PORTAL;
                comprobante.ALIAS_PORTAL = this.ALIAS_PORTAL;
                comprobante.CLAVE_INTERBANCARIA = this.CLAVE_INTERBANCARIA;
                comprobante.REFERENCIA_RAP = this.REFERENCIA_RAP;
                comprobante.ESPORADICO = this.ESPORADICO;
                comprobante.MERCADO = this.MERCADO;
                comprobante.FOLIO_VERIFICACION = this.FOLIO_VERIFICACION;
                comprobante.PROVEEDORES = this.PROVEEDORES;
                comprobante.GRUPO_ID = this.GRUPO_ID;

                //comprobante.DIRECCION = new DIRECCION
                //                                {
                //                                    CALLE_No = this.DIRECCION.CALLE_No,
                //                                    COLONIA = this.DIRECCION.COLONIA,
                //                                    MUNICIPIO = this.DIRECCION.MUNICIPIO,
                //                                    ESTADO = this.DIRECCION.ESTADO,
                //                                    RFC = this.RFC,
                //                                    CP = this.DIRECCION.CP
                //                                };
                foreach (var d in CONOCIMIENTO_PV)
                {
                    comprobante.CONOCIMIENTO_PV.Add(new CONOCIMIENTO_PV
                    {
                        TIPO = d.TIPO,
                        DIRECCION = d.DIRECCION//,
                        //CREATEDATE = DateTime.Now
                    });
                }

                return comprobante;
            }





        }
        #endregion
    }
}
