namespace Clientes.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("EMPRESARIALES.CONOCIMIENTO_PV")]
    public partial class CONOCIMIENTO_PV
    {
        public int ID { get; set; }

        [StringLength(175)]
        public string DIRECCION { get; set; }

        [StringLength(13)]
        public string TIPO { get; set; }

        public DateTime? CREATEDATE { get { return DateTime.Now;}}

        public int CONOCIMIENTO_ID { get; set; }

        public virtual CONOCIMIENTO CONOCIMIENTO { get; set; }
    }


    #region ViewModels
    public partial class CONOCIMIENTO_PVViewModel
    {
        public string TIPO { get; set; }
        public string DIRECCION { get; set; }
        public bool Retirar { get; set; }
        
    }
    #endregion
}
