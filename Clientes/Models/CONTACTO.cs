﻿namespace Clientes.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EMPRESARIALES.CONTACTO")]
    public partial class CONTACTO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CONTACTO()
        {
            COMPROMISOes = new HashSet<COMPROMISO>();
            PROXIMO_CONTACTO = new HashSet<PROXIMO_CONTACTO>();
        }

        [Key]
        public int ID { get; set; }

        public int CONOCIMIENTO_ID { get; set; }

        [StringLength(100)]
        public string NOMBRE_CONTACTO { get; set; }

        [StringLength(50)]
        public string TIPO_CONTACTO { get; set; }

        //Crear textarea
        [DataType(DataType.MultilineText)]
        [Column(TypeName = "text")]
        public string COMENTARIO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<COMPROMISO> COMPROMISOes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PROXIMO_CONTACTO> PROXIMO_CONTACTO { get; set; }


        public DateTime FECHA_CONTACTO { get; set; }
    }
}
