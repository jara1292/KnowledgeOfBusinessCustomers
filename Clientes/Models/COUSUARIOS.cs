﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clientes.Models
{
    [Table("EMPRESARIALES.COUSUARIOS")]
    public partial class COUSUARIOS
    {
        [Key]
        public int ID { get; set; }

        public string VENDEDOR { get; set; }

        public string NOMBRE { get; set; }

        public string PASSWORD { get; set; }

        public bool ACTIVO { get; set; }

        public int PERFIL { get; set; }
    }
}