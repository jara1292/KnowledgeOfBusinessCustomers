﻿using Foolproof;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Clientes.Models
{
    public class ContactoModelView
    {
        public int CONOCIMIENTO_ID { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        [StringLength(100)]
        [Display(Name = "Nombre de contacto")]
        public string NOMBRE_CONTACTO { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        [StringLength(50)]
        [Display(Name = "Tipo de contacto")]
        public string TIPO_CONTACTO { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Comentario")]
        [Column(TypeName = "text")]
        public string COMENTARIO { get; set; }

        [Display(Name = "Fecha")]
        public DateTime FECHA_CONTACTO { get; set; }


        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<COMPROMISO> COMPROMISOes { get; set; }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<PROXIMO_CONTACTO> PROXIMO_CONTACTO { get; set; }

        //[NotMapped]
        [Display(Name = "Agregar compromiso?")]
        public string AgregarCompromiso { get; set; }



        [RequiredIf("AgregarCompromiso", "s", ErrorMessage = "Campo requerido")]
        [Display(Name = "Compromiso")]
        [Column("COMPROMISO", TypeName = "text")]
        public string COMPROMISO { get; set; }

        [RequiredIf("AgregarCompromiso", "s", ErrorMessage = "Campo requerido")]
        [Display(Name = "Fecha compromiso")]
        [Column(TypeName = "date")]
        public DateTime? FECHA_COMPROMISO { get; set; }


        [RequiredIf("AgregarCompromiso", "n", ErrorMessage = "Campo requerido")]
        [Display(Name = "Motivo de proximo contacto")]
        [Column(TypeName = "text")]
        public string MOTIVO_PROXIMO_CONTACTO { get; set; }

        [RequiredIf("AgregarCompromiso", "n", ErrorMessage = "Campo requerido")]
        [Display(Name = "fecha proximo contacto")]
        //[Column(TypeName = "date")]
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm}", ApplyFormatInEditMode = true)]
        public string FECHA_PROXIMO_CONTACTO { get; set; }

        //[RequiredIf("AgregarCompromiso", "n", ErrorMessage = "Campo requerido")]
        [Display(Name = "Hora proximo contacto")]
        public TimeSpan? HORA_PROXIMO_CONTACTO { get; set; }


        public string OTRO_CONTACTO { get; set; }
    }
}