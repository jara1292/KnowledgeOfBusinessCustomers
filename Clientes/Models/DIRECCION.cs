﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Clientes.Models
{
    [Table("EMPRESARIALES.DIRECCION")]
    public class DIRECCION
    {
        public string CALLE_No { get; set; }

        public string COLONIA { get; set; }

        public string MUNICIPIO { get; set; }

        public string ESTADO { get; set; }

        public string CP { get; set; }

        [Key]
        public int ID { get; set; }

        public string RFC { get; set; }

        public string TIPO { get; set; }

        public int CONOCIMIENTO_ID { get; set; }

        //public virtual CONOCIMIENTO CONOCIMIENTO { get; set; }
    }



    #region ViewModels
    public partial class DireccionViewModel
    {
        public string CALLE_No { get; set; }
        public string COLONIA { get; set; }
        public string MUNICIPIO { get; set; }
        public string ESTADO { get; set; }
        public string CP { get; set; }
        public string RFC { get; set; }

    }
    #endregion
}