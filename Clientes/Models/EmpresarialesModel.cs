namespace Clientes.Models
{
    using System.Data.Entity;

    public partial class EmpresarialesModel : DbContext
    {
        public EmpresarialesModel()
            : base("name=EmpresarialesModel")
        {
        }

        public virtual DbSet<CONOCIMIENTO> CONOCIMIENTO { get; set; }
        public virtual DbSet<DIRECCION> DIRECCION { get; set; }
        public virtual DbSet<CONOCIMIENTO_PV> CONOCIMIENTO_PV { get; set; }
        public virtual DbSet<SITUACION> SITUACION { get; set; }
        public virtual DbSet<COUSUARIOS> COUSUARIOS { get; set; }


        public virtual DbSet<COMPROMISO> COMPROMISOes { get; set; }
        public virtual DbSet<CONTACTO> CONTACTOes { get; set; }
        public virtual DbSet<PROXIMO_CONTACTO> PROXIMO_CONTACTO { get; set; }
        public virtual DbSet<GRUPO> GRUPOS { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<COUSUARIOS>()
               .Property(e => e.VENDEDOR)
               .IsUnicode(false);

            modelBuilder.Entity<COUSUARIOS>()
               .Property(e => e.NOMBRE)
               .IsUnicode(false);

            modelBuilder.Entity<COUSUARIOS>()
               .Property(e => e.PASSWORD)
               .IsUnicode(false);

            modelBuilder.Entity<COUSUARIOS>()
               .Property(e => e.ACTIVO);

            modelBuilder.Entity<COUSUARIOS>()
               .Property(e => e.PERFIL);

            //modelBuilder.Entity<CONOCIMIENTO>()
            //    .Property(e => e.TIPO)
            //    .IsUnicode(false);

            modelBuilder.Entity<CONOCIMIENTO>()
                .Property(e => e.RAZON_SOCIAL)
                .IsUnicode(false);

            modelBuilder.Entity<CONOCIMIENTO>()
                .Property(e => e.NOMBRE_COMERCIAL)
                .IsUnicode(false);

            modelBuilder.Entity<CONOCIMIENTO>()
                .Property(e => e.RFC)
                .IsUnicode(false);

            modelBuilder.Entity<CONOCIMIENTO>()
                .Property(e => e.DESC_ACTIVIDAD)
                .IsUnicode(false);

            modelBuilder.Entity<CONOCIMIENTO>()
                .Property(e => e.COMPETENCIA)
                .IsUnicode(false);

            modelBuilder.Entity<CONOCIMIENTO>()
                .Property(e => e.PRINCIPALES_CLIENTES)
                .IsUnicode(false);

            modelBuilder.Entity<CONOCIMIENTO>()
                .Property(e => e.PAGINA)
                .IsUnicode(false);

            modelBuilder.Entity<CONOCIMIENTO>()
                .Property(e => e.TEL_EMPRESA)
                .IsUnicode(false);

            //modelBuilder.Entity<CONOCIMIENTO>()
            //    .Property(e => e.DIRECCION)
            //    .IsUnicode(false);

            modelBuilder.Entity<CONOCIMIENTO>()
                .Property(e => e.REP_LEGAL)
                .IsUnicode(false);

            modelBuilder.Entity<CONOCIMIENTO>()
                .Property(e => e.CONTACTO_ALIADO)
                .IsUnicode(false);

            modelBuilder.Entity<CONOCIMIENTO>()
                .Property(e => e.CARGO_ALIADO)
                .IsUnicode(false);

            modelBuilder.Entity<CONOCIMIENTO>()
                .Property(e => e.CONTACTO_TECNICO)
                .IsUnicode(false);

            modelBuilder.Entity<CONOCIMIENTO>()
                .Property(e => e.CARGO_TECNICO)
                .IsUnicode(false);

            modelBuilder.Entity<CONOCIMIENTO>()
                .Property(e => e.CONTACTO_PRINCIPAL)
                .IsUnicode(false);

            modelBuilder.Entity<CONOCIMIENTO>()
                .Property(e => e.CARGO_PRINCIPAL)
                .IsUnicode(false);

            modelBuilder.Entity<CONOCIMIENTO>()
                .Property(e => e.CONTACTO_CXP)
                .IsUnicode(false);

            modelBuilder.Entity<CONOCIMIENTO>()
                .Property(e => e.CARGO_CXP)
                .IsUnicode(false);

            modelBuilder.Entity<CONOCIMIENTO>()
                .Property(e => e.EJECUTIVO_FONIX)
                .IsUnicode(false);

            modelBuilder.Entity<CONOCIMIENTO>()
                .HasMany(e => e.CONOCIMIENTO_PV)
                .WithRequired(e => e.CONOCIMIENTO)
                .HasForeignKey(e => e.CONOCIMIENTO_ID)
                .WillCascadeOnDelete(false);

            //modelBuilder.Entity<DIRECCION>()
            //    .HasRequired(e => e.CONOCIMIENTO)
            //    .WithOptional(e => e.DIRECCION);

            modelBuilder.Entity<CONOCIMIENTO_PV>()
                .Property(e => e.DIRECCION)
                .IsUnicode(false);

            modelBuilder.Entity<CONOCIMIENTO_PV>()
                .Property(e => e.TIPO)
                .IsUnicode(false);

            modelBuilder.Entity<SITUACION>()
                .Property(e => e.FACT_MENSUAL)
                .HasPrecision(19, 4);

            modelBuilder.Entity<SITUACION>()
                .Property(e => e.DATOS_PUROS)
                .IsUnicode(false);

            modelBuilder.Entity<SITUACION>()
                .Property(e => e.TABLETAS)
                .IsUnicode(false);

            modelBuilder.Entity<SITUACION>()
                .Property(e => e.BANDAS_ANCHAS)
                .IsUnicode(false);

            modelBuilder.Entity<SITUACION>()
                .Property(e => e.LOCALIZACION)
                .IsUnicode(false);

            modelBuilder.Entity<SITUACION>()
                .Property(e => e.OTROS)
                .IsUnicode(false);

            modelBuilder.Entity<SITUACION>()
                .Property(e => e.PROBLEMA)
                .IsUnicode(false);

            modelBuilder.Entity<SITUACION>()
                .Property(e => e.IMPLICACION)
                .IsUnicode(false);

            modelBuilder.Entity<SITUACION>()
                .Property(e => e.SOLUCION)
                .IsUnicode(false);

            modelBuilder.Entity<SITUACION>()
                .Property(e => e.CUANTO_TELEFONOS)
                .IsUnicode(false);

            modelBuilder.Entity<SITUACION>()
                .Property(e => e.CUANTO_SERVICIOS)
                .IsUnicode(false);


            //modelBuilder.Entity<CONTACTO>()
            //    .Property(e => e.NOMBRE_CONTACTO)
            //    .IsUnicode(false);

            //modelBuilder.Entity<CONTACTO>()
            //    .Property(e => e.COMENTARIO)
            //    .IsUnicode(false);


            modelBuilder.Entity<COMPROMISO>()
                .Property(e => e.COMPROMISO1)
                .IsUnicode(false);

            modelBuilder.Entity<CONTACTO>()
                .Property(e => e.NOMBRE_CONTACTO)
                .IsUnicode(false);

            modelBuilder.Entity<CONTACTO>()
                .Property(e => e.TIPO_CONTACTO)
                .IsUnicode(false);

            modelBuilder.Entity<CONTACTO>()
                .Property(e => e.COMENTARIO)
                .IsUnicode(false);

            modelBuilder.Entity<CONTACTO>()
                .HasMany(e => e.COMPROMISOes)
                .WithOptional(e => e.CONTACTO)
                .HasForeignKey(e => e.CONTACTO_ID);

            modelBuilder.Entity<CONTACTO>()
                .HasMany(e => e.PROXIMO_CONTACTO)
                .WithOptional(e => e.CONTACTO)
                .HasForeignKey(e => e.CONTACTO_ID);

            modelBuilder.Entity<PROXIMO_CONTACTO>()
                .Property(e => e.MOTIVO_PROXIMO_CONTACTO)
                .IsUnicode(false);
        }

        public System.Data.Entity.DbSet<Clientes.Models.ADENDA> ADENDAs { get; set; }
    }
}
