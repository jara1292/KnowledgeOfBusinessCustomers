﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clientes.Models
{
    public class Events
    {

        public int id { get; set; }

        public string start { get; set; }

        //public string end { get; set; }

        public string title { get; set; }

        public string color { get; set; }

        public string className { get; set; }

        public string ejecutivo {get;set;}

        public string tipo { get; set; }

        public string estatus { get; set; }

        public bool allDay { get; set; }
    }
}