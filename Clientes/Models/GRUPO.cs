﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Clientes.Models
{
    [Table("EMPRESARIALES.GRUPO")]
    public class GRUPO
    {
        [Key]
        public int ID { get; set; }

        [Display(Name="Grupo")]
        public string RAZON { get; set; }
    }
}