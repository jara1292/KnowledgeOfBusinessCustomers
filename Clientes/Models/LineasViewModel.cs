﻿using System;

namespace Clientes.Models
{
    public class LineasViewModel
    {
        public string TIPO { get; set; }
        public string REGION { get; set; }
        public string CUENTA_PADRE { get; set; }
        public string CUENTA { get; set; }
        public int? CICLO { get; set; }
        public string RFC { get; set; }
        public string RAZON_SOCIAL { get; set; }
        public string TELEFONO { get; set; }
        public string ICCID { get; set; }
        public string IMEI { get; set; }
        public string ESTATUS_CUENTA { get; set; }
        public string PRODUCTO_CC { get; set; }
        public string CLAVE_PLAN { get; set; }
        public string NOMBRE_PLAN { get; set; }
        public decimal? MONTO_RENTA { get; set; }
        public string EQUIPO { get; set; }
        public string MODELO { get; set; }
        public int? DURACION { get; set; }
        public DateTime? FECHA_INICIO { get; set; }
        public DateTime? FECHA_TERMINO { get; set; }
        public string ESTATUS_ADENDUM { get; set; }
        public string ESTATUS_LINEA { get; set; }
        public int? MESES_RESTANTES { get; set; }
        public string EJECUTIVO { get; set; }
    }
}