namespace Clientes.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EMPRESARIALES.PROXIMO_CONTACTO")]
    public partial class PROXIMO_CONTACTO
    {
        [Key]
        public int ID { get; set; }

        public int? CONTACTO_ID { get; set; }

        [Column(TypeName = "text")]
        [Display(Name = "Motivo proximo contacto")]
        //Crear textarea
        [DataType(DataType.MultilineText)]
        [Required]
        public string MOTIVO_PROXIMO_CONTACTO { get; set; }

        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm}", ApplyFormatInEditMode = true)]
        [Display(Name = "Fecha proximo contacto")]
        [Required]
        public DateTime? FECHA_PROXIMO_CONTACTO { get; set; }

        [Display(Name = "Hora proximo contacto")]
        //[Required]
        public TimeSpan? HORA_PROXIMO_CONTACTO { get; set; }

        public virtual CONTACTO CONTACTO { get; set; }
    }
}
