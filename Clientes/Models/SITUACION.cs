namespace Clientes.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("EMPRESARIALES.SITUACION")]
    public partial class SITUACION
    {
        [Display(Name = "Cantidad de l�neas que factura al mes")]
        public int? CANTIDAD_LINEAS { get; set; }

        [Display(Name = "Facturaci�n mensual en pesos")]
        [Column(TypeName = "money")]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        public decimal? FACT_MENSUAL { get; set; }



        public int? TELCEL_LINEAS { get; set; }

        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime? TELCEL_FECHA { get; set; }

        public int? ATT_LINEAS { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        [Column(TypeName = "date")]
        public DateTime? ATT_FECHA { get; set; }

        public int? MOVISTAR_LINEAS { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        [Column(TypeName = "date")]
        public DateTime? MOVISTAR_FECHA { get; set; }

        [StringLength(150)]
        [Display(Name = "Datos puros")]
        public string DATOS_PUROS { get; set; }


        [StringLength(150)]
        public string TABLETAS { get; set; }

        [Display(Name = "Bandas anchas")]
        [StringLength(150)]
        public string BANDAS_ANCHAS { get; set; }

        [Display(Name = "Localizaci�n")]
        [StringLength(150)]
        public string LOCALIZACION { get; set; }

        [StringLength(150)]
        public string OTROS { get; set; }

        [StringLength(200)]
        public string PROBLEMA { get; set; }

        [StringLength(200)]
        public string IMPLICACION { get; set; }

        [StringLength(200)]
        public string SOLUCION { get; set; }

        [Display(Name = "En cuanto a tel�fonos")]
        [StringLength(200)]
        public string CUANTO_TELEFONOS { get; set; }


        [Display(Name = "En cuanto a servicios")]
        [StringLength(200)]
        public string CUANTO_SERVICIOS { get; set; }

        public DateTime? CREATEDATE { get {return DateTime.Now; } }

        [Key]
        public int ID { get; set; }


        public int? IDCONOCIMIENTO { get; set; }


        public string DATOS_TELCEL { get; set; }
        public string DATOS_MOVISTAR { get; set; }
        public string DATOS_ATT { get; set; }

        public int? MINUTOS_TELCEL { get; set; }
        public int? MINUTOS_MOVISTAR { get; set; }
        public int? MINUTOS_ATT { get; set; }

        public decimal? PAGO_MENSUAL_TELCEL { get; set; }
        public decimal? PAGO_MENSUAL_MOVISTAR { get; set; }
        public decimal? PAGO_MENSUAL_ATT { get; set; }


        //public virtual CONOCIMIENTO CONOCIMIENTO { get; set; }
    }
}
