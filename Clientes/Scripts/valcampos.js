﻿$(document).ready(function () {
    //Codigo que valida los campos del formulario de solicitudes con el plugin Validate JQuery
    $("#form").validate({
        rules: {
            FECHA_ATUALIZACION: {
                required: true
            },
            RAZON_SOCIAL: {
                required: true
            },
            NOMBRE_COMERCIAL: {
                required: true,
            },
            RFC: {
                required: true,
            },
            GIRO: {
                required: true
            },
            ANIOS_MERCADO: {
                required: true
            },
            DESC_ACTIVIDAD: {
                required: true
            },
            EJECUTIVO_FONIX: {
                required: true
            },
        },
        messages: {
            FECHA_ATUALIZACION: {
                required: "Campo requerido"
            },
            RAZON_SOCIAL: {
                required: "Campo requerido"
            },
            RFC: {
                required: "Campo requerido"
            },
            NOMBRE_COMERCIAL: {
                required: "Campo requerido",
            },
            GIRO: {
                required: "Campo requerido"
            },
            ANIOS_MERCADO: {
                required: "Campo requerido"
            },
            DESC_ACTIVIDAD: {
                required: "Campo requerido"
            },
            EJECUTIVO_FONIX: {
                required: "Campo requerido"
            }
        },
        errorElement: "span"
    });
});