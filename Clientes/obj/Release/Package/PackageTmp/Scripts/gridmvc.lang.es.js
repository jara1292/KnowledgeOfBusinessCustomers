﻿/***
* Grid.Mvc french language (fr-FR) http://gridmvc.codeplex.com/
*/
window.GridMvc = window.GridMvc || {};
window.GridMvc.lang = window.GridMvc.lang || {};
GridMvc.lang.es = {
    filterTypeLabel: "Tipo: ",
    filterValueLabel: "Valor :",
    applyFilterButtonText: "Aplicar",
    filterSelectTypes: {
        Equals: "Igual",
        StartsWith: "Comienza por",
        Contains: "Contiene",
        EndsWith: "Termina con",
        GreaterThan: "Superior a",
        LessThan: "Inferior a",
        GreaterThanOrEquals: "Mayor que o igual a",
        LessThanOrEquals: "Inferior o igual a",
    },
    code: 'es',
    boolTrueLabel: "Si",
    boolFalseLabel: "No",
    clearFilterLabel: "Borrar filtro"
};